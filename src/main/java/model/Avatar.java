package model;

import javax.persistence.*;

/**
 * Avatar for owner
 */
@Entity(name = "AVATAR")
public class Avatar {

    /* Declare attributes */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Lob
    @Column(name = "IMAGE", columnDefinition="BLOB")
    private byte[] image;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FRK_OWNER_ID")
    private Owner owner;

    /* Constructor */
    public Avatar() {}

    /* Getters and Setters */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
