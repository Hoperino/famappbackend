package model.utils;

/**
 * Enum for payment types
 */
public enum PaymentType {
    INCOME,
    TAX,
    OTHER,
    UTILITY
}
