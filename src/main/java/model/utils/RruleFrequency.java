package model.utils;

/**
 * Used by Recurring event
 */
public enum RruleFrequency {
    YEARLY,
    MONTHLY
}
