package model.utils;

/**
 * Enum for asset types
 */
public enum AssetType {
    REAL_ESTATE,
    STOCK,
    INSURANCE,
    VEHICLE,
    RECURRING_EVENT
}
