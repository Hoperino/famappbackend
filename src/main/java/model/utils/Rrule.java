package model.utils;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Used in Recurring event
 */
@Embeddable
public class Rrule {

    /* Define attributes */
    @Column(name = "FREQUENCY")
    private RruleFrequency frequency;

    @Column(name = "MONTH_DAY")
    private int byMonthDay;

    @Column(name = "MONTH")
    private int byMonth;

    /* Constructor */
    public Rrule(RruleFrequency frequency, int byMonth, int byMonthDay) {
        this.frequency = frequency;
        this.byMonth = byMonth;
        this.byMonthDay = byMonthDay;
    }

    /* Getters and Setters */
    public RruleFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(RruleFrequency frequency) {
        this.frequency = frequency;
    }

    public int getByMonthDay() {
        return byMonthDay;
    }

    public void setByMonthDay(int byMonthDay) {
        this.byMonthDay = byMonthDay;
    }

    public int getByMonth() {
        return byMonth;
    }

    public void setByMonth(int byMonth) {
        this.byMonth = byMonth;
    }

    /* Override toString() */
    @Override
    public String toString() {
        return "Rrule{" +
                "frequency=" + frequency +
                ", byMonthDay=" + byMonthDay +
                ", byMonth=" + byMonth +
                '}';
    }
}
