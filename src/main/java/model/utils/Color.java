package model.utils;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Color for calendar, used in Recurring event
 */
@Embeddable
public class Color {

    /* Define attributes */
    @Column(name = "PRIMARY_COLOR")
    private String primary;

    @Column(name = "SECONDARY_COLOR")
    private String secondary;

    /* Constructor */
    public Color(String primary, String secondary) {
        this.primary = primary;
        this.secondary = secondary;
    }

    /* Getters and Setters */
    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getSecondary() {
        return secondary;
    }

    public void setSecondary(String secondary) {
        this.secondary = secondary;
    }

    /* Override toString() */
    @Override
    public String toString() {
        return "Color{" +
                "primary='" + primary + '\'' +
                ", secondary='" + secondary + '\'' +
                '}';
    }
}