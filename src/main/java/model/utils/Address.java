package model.utils;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Address model
 */
@Embeddable
public class Address {

    /* Define attributes */
    @Column(name = "STREET_NAME")
    private String streetName;

    @Column(name = "CITY")
    private String city;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "POST_CODE")
    private int postCode;

    /* Constructor */
    public Address() {}

    /* Getters and Setters */

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    /* Override toString() */
    @Override
    public String toString() {
        return "Address{" +
                "streetName='" + streetName + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", postCode=" + postCode +
                '}';
    }
}
