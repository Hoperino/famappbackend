package model.utils;

/**
 * Enum for document types
 */
public enum DocumentType {
    PAYMENT,
    DESCRIPTIVE
}
