package model.property;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Vehicle model
 */
@Entity
@DiscriminatorValue("1")
public class Vehicle extends Property {

    /* Define attributes */
    @Column(name = "TYPE")
    private String type;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "YEAR")
    private int year;

    /* Constructor */
    public Vehicle() {}

    /* Getters and Setters */
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /* toString() */
    @Override
    public String toString() {
        return "Vehicle{" +
                "  id=" + this.getId() +
                ", name='" + this.getName() + '\'' +
                ", datePurchased=" + this.getDatePurchased() +
                ", price=" + this.getPrice() +
                ", purchasedFrom='" + this.getPurchasedFrom() + '\'' +
                ", description='" + this.getDescription() + '\'' +
                ", isArchived=" + this.isArchived() +
                ", owner=" + this.getOwner() +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                '}';
    }
}
