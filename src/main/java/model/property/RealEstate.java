package model.property;

import model.utils.Address;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

/**
 * Real Estate model
 */
@Entity
@DiscriminatorValue("2")
public class RealEstate extends Property {

    /* Define attributes */
    @Column(name = "IS_LAND")
    private boolean isLand;

    @Column(name = "SIZE")
    private int size;

    @Embedded
    private Address address;

    @Column(name = "RENT_YEAR")
    private double rentYear;

    @Column(name = "RENTER_NAME")
    private String renterName;

    /* Constructor */
    public RealEstate() {}

    /* Getters and Setters */
    public boolean isLand() {
        return isLand;
    }

    public void setLand(boolean land) {
        isLand = land;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public double getRentYear() {
        return rentYear;
    }

    public void setRentYear(double rentYear) {
        this.rentYear = rentYear;
    }

    public String getRenterName() {
        return renterName;
    }

    public void setRenterName(String renterName) {
        this.renterName = renterName;
    }

    /* toString() */
    @Override
    public String toString() {
        return "RealEstate{" +
                "  id=" + this.getId() +
                ", name='" + this.getName() + '\'' +
                ", datePurchased=" + this.getDatePurchased() +
                ", price=" + this.getPrice() +
                ", purchasedFrom='" + this.getPurchasedFrom() + '\'' +
                ", description='" + this.getDescription() + '\'' +
                ", isArchived=" + this.isArchived() +
                ", owner=" + this.getOwner() +
                ", isLand=" + isLand +
                ", size=" + size +
                ", address=" + address +
                ", documentSet=" + this.getDocumentSet() +
                ", paymentSet=" + this.getPaymentSet() +
                '}';
    }
}
