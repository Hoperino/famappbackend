package model.property;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import model.Document;
import model.Owner;
import model.Payment;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Base class for Property
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.PROPERTY, property="classType")
@Entity(name = "PROPERTIES")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "DISCRIMINATOR",
        discriminatorType = DiscriminatorType.INTEGER
)
public abstract class Property {

    /* Define attributes */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "DATE_PURCHASED")
    private Date datePurchased;

    @Column(name = "PRICE")
    private double price;

    @Column(name = "PURCHASED_FROM")
    private String purchasedFrom;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IS_ARCHIVED")
    private boolean isArchived;

    @ManyToOne
    @JoinColumn(name = "OWNER_ID",nullable = false)
    private Owner owner;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.REMOVE})
    @JoinTable(
            name = "PROPERTIES_TO_DOCUMENTS",
            joinColumns = { @JoinColumn(name = "PROPERTY_ID") },
            inverseJoinColumns = { @JoinColumn(name = "DOCUMENT_ID", unique = true) }
    )
    private Set<Document> documentSet = new HashSet<>();

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "PROPERTIES_TO_PAYMENTS",
            joinColumns = { @JoinColumn(name = "PROPERTY_ID") },
            inverseJoinColumns = { @JoinColumn(name = "PAYMENT_ID", unique = true) }
    )
    private Set<Payment> paymentSet = new HashSet<>();

    /* Constructor */
    protected Property() {}

    /* Getters and Setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDatePurchased() {
        return datePurchased;
    }

    public void setDatePurchased(Date datePurchased) {
        this.datePurchased = datePurchased;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPurchasedFrom() {
        return purchasedFrom;
    }

    public void setPurchasedFrom(String purchasedFrom) {
        this.purchasedFrom = purchasedFrom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Set<Document> getDocumentSet() {
        return documentSet;
    }

    public void setDocumentSet(Set<Document> documentSet) {
        this.documentSet = documentSet;
    }

    public Set<Payment> getPaymentSet() {
        return paymentSet;
    }

    public void setPaymentSet(Set<Payment> paymentSet) {
        this.paymentSet = paymentSet;
    }

    /* Override equals(), hasCode() and toString() */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Property property = (Property) o;

        return getId().equals(property.getId()) && getName().equals(property.getName());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getName().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Property{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", datePurchased=" + datePurchased +
                ", price=" + price +
                ", purchasedFrom='" + purchasedFrom + '\'' +
                ", description='" + description + '\'' +
                ", isArchived=" + isArchived +
                ", owner=" + owner +
                ", documentSet=" + documentSet +
                ", paymentSet=" + paymentSet +
                '}';
    }
}
