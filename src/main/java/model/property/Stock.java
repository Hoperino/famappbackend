package model.property;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Stock model
 */
@Entity
@DiscriminatorValue("3")
public class Stock extends Property {

    /* Define attributes */
    @Column(name = "COMPANY")
    private String company;

    @Column(name = "NUMBER_OF_SHARED")
    private int numberOfShares;

    @Column(name = "YEARLY_DIVIDEND")
    private double yearlyDividend;

    /* Constructor */
    public Stock() {}

    /* Getters and Setters */
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getNumberOfShares() {
        return numberOfShares;
    }

    public void setNumberOfShares(int numberOfShares) {
        this.numberOfShares = numberOfShares;
    }

    public double getYearlyDividend() {
        return yearlyDividend;
    }

    public void setYearlyDividend(double yearlyDividend) {
        this.yearlyDividend = yearlyDividend;
    }

    /* toString() */
    @Override
    public String toString() {
        return "Stock{" +
                "  id=" + this.getId() +
                ", name='" + this.getName() + '\'' +
                ", datePurchased=" + this.getDatePurchased() +
                ", price=" + this.getPrice() +
                ", purchasedFrom='" + this.getPurchasedFrom() + '\'' +
                ", description='" + this.getDescription() + '\'' +
                ", isArchived=" + this.isArchived() +
                ", owner=" + this.getOwner() +
                ", company='" + company + '\'' +
                ", numberOfShares=" + numberOfShares +
                ", yearlyDividend=" + yearlyDividend +
                ", documentSet=" + this.getDocumentSet() +
                ", paymentSet=" + this.getPaymentSet() +
                '}';
    }
}
