package model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Insurance model
 */
@Entity(name = "INSURANCE")
public class Insurance {

    /* Define attributes */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "UID")
    private String uid;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TARGET")
    private String target;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ISSUER")
    private String issuer;

    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "ISSUE_DATE")
    private Date issueDate;

    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "COVERAGE")
    private double coverage;

    @Column(name = "IS_ARCHIVED")
    private boolean isArchived;

    @ManyToOne
    @JoinColumn(name = "FRK_OWNER_ID")
    private Owner owner;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "INSURANCES_TO_PAYMENTS",
            joinColumns = { @JoinColumn(name = "INSURANCE_ID") },
            inverseJoinColumns = { @JoinColumn(name = "PAYMENT_ID",
                    unique = true)
            }
    )
    private Set<Payment> paymentSet = new HashSet<>();

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "INSURANCES_TO_DOCUMENTS",
            joinColumns = { @JoinColumn(
                    name = "INSURANCE_ID") },
            inverseJoinColumns = { @JoinColumn(name = "DOCUMENT_ID",
                    unique = true)
            }
    )
    private Set<Document> documentSet = new HashSet<>();


    /* Constructor */
    public Insurance() {}

    /* Getters and Setters */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getCoverage() {
        return coverage;
    }

    public void setCoverage(double coverage) {
        this.coverage = coverage;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public Set<Payment> getPaymentSet() {
        return paymentSet;
    }

    public void setPaymentSet(Set<Payment> paymentSet) {
        this.paymentSet = paymentSet;
    }

    public Set<Document> getDocumentSet() {
        return documentSet;
    }

    public void setDocumentSet(Set<Document> documentSet) {
        this.documentSet = documentSet;
    }

    /* Override equals(), hasCode() and toString() */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Insurance insurance = (Insurance) o;

        return getId().equals(insurance.getId()) && getUid().equals(insurance.getUid()) && getName().equals(insurance.getName());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getUid().hashCode();
        result = 31 * result + getName().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Insurance{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", target='" + target + '\'' +
                ", description='" + description + '\'' +
                ", issuer='" + issuer + '\'' +
                ", issueDate=" + issueDate +
                ", endDate=" + endDate +
                ", coverage=" + coverage +
                ", owner=" + owner +
                ", isArchived=" + isArchived +
                '}';
    }
}
