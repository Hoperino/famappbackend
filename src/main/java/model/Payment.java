package model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import model.property.Property;
import model.utils.PaymentType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Payment model
 */
@Entity(name = "PAYMENT")
public class Payment {

    /* Define attributes */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "VALUE")
    private double value;

    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Column(name = "DATE_ISSUED")
    private Date dateIssued;

    @Column(name = "ISSUER")
    private String issuer;

    @Column(name = "RECEIVER")
    private String receiver;

    @Column(name = "TYPE")
    private PaymentType type;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IS_ARCHIVED")
    private boolean isArchived;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "paymentSet")
    private Set<Insurance> insuranceSet = new HashSet<>();

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "paymentSet")
    private Set<Property> recurringEventSet = new HashSet<>();

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(mappedBy = "paymentSet")
    private Set<Property> propertySet = new HashSet<>();

    /* Constructor */
    public Payment() {}

    /* Getters and Setters */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(Date dateIssued) {
        this.dateIssued = dateIssued;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public PaymentType getType() {
        return type;
    }

    public void setType(PaymentType type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public Set<Insurance> getInsuranceSet() {
        return insuranceSet;
    }

    public void setInsuranceSet(Set<Insurance> insuranceSet) {
        this.insuranceSet = insuranceSet;
    }

    public Set<Property> getRecurringEventSet() {
        return recurringEventSet;
    }

    public void setRecurringEventSet(Set<Property> recurringEventSet) {
        this.recurringEventSet = recurringEventSet;
    }

    public Set<Property> getPropertySet() {
        return propertySet;
    }

    public void setPropertySet(Set<Property> propertySet) {
        this.propertySet = propertySet;
    }

    /* Override equals(), hasCode() and toString() */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        return Double.compare(payment.getValue(), getValue()) == 0 && getId().equals(payment.getId())
                && getName().equals(payment.getName()) && getType() == payment.getType();
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getId().hashCode();
        result = 31 * result + getName().hashCode();
        temp = Double.doubleToLongBits(getValue());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + getType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", dateIssued=" + dateIssued +
                ", issuer='" + issuer + '\'' +
                ", receiver='" + receiver + '\'' +
                ", types=" + type +
                ", reason='" + reason + '\'' +
                ", description='" + description + '\'' +
                ", isArchived=" + isArchived +
                '}';
    }
}
