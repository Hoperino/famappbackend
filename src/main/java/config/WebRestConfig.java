package config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Web Rest Configurer
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "web.rest")
public class WebRestConfig extends WebMvcConfigurerAdapter {

    @Bean
    public UtilityRestMethods getUtilityRestMethods() {
        return UtilityRestMethods.getInstance();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/resources/**")
                .addResourceLocations("/resources/")
                .setCachePeriod(3600)
                .resourceChain(false) // true when deployed
                .addResolver(new PathResourceResolver());
    }

    @Override
    public void configureMessageConverters(
            List<HttpMessageConverter<?>> converters) {

        MappingJackson2HttpMessageConverter jackson2Converter = new MappingJackson2HttpMessageConverter();
        jackson2Converter.setObjectMapper(new HibernateAwareObjectMapper());
        converters.add(jackson2Converter);

        super.configureMessageConverters(converters);
    }

    private class HibernateAwareObjectMapper extends ObjectMapper {
        public HibernateAwareObjectMapper() {
            registerModule(new Hibernate5Module());

        /*
        * NOT NEEDED
        *   this.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        *   this.enableDefaultTyping();
        * */

        }
    }
}

