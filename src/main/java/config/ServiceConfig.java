package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Service configuration class
 */
@Configuration
@ComponentScan("service.hibernate")
public class ServiceConfig {
}
