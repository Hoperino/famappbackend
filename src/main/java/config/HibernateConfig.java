package config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.annotation.PostConstruct;
import java.util.Properties;


/**
 * Configure hibernate
 */
@Configuration
@PropertySource({ "classpath:properties/mysql.properties" })
public class HibernateConfig {

    private String driverClassName;
    private String url;
    private String username;
    private String password;

    private String hibernateDialect;
    private String hibernateShowSql;
    private String hibernateHbm2ddlAuto;
    private String hibernateConnectionCharSet;
    private String hibernateConnectionCharacterEncoding;
    private String hibernateConnectionUseUnicode;

    public HibernateConfig() {
    }

    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        driverClassName = env.getProperty("jdbc.mysql.driverClassName");
        url = env.getProperty("jdbc.mysql.url");
        username = env.getProperty("jdbc.mysql.user");
        password = env.getProperty("jdbc.mysql.pass");

        hibernateDialect = env.getProperty("hibernate.mysql.dialect");
        hibernateShowSql = env.getProperty("hibernate.mysql.show_sql");
        hibernateHbm2ddlAuto = env.getProperty("hibernate.mysql.hbm2ddl.auto");
        hibernateConnectionCharSet = env.getProperty("hibernate.connection.charSet");
        hibernateConnectionCharacterEncoding = env.getProperty("hibernate.connection.characterEncoding");
        hibernateConnectionUseUnicode = env.getProperty("hibernate.connection.useUnicode");

    }

    @Bean
    public BasicDataSource getBasicSource() {

        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driverClassName);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);

        return ds;
    }

    @Bean
    public LocalSessionFactoryBean getLocalSessionFactory() {

        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(getBasicSource());
        sessionFactoryBean.setPackagesToScan("model");
        sessionFactoryBean.setHibernateProperties(getHibernateProperties());

        return sessionFactoryBean;
    }

    @Bean(name = "sessionFactory")
    public Properties getHibernateProperties()
    {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", hibernateDialect);
        properties.put("hibernate.show_sql", hibernateShowSql);
        properties.put("hibernate.hbm2ddl.auto", hibernateHbm2ddlAuto);
        properties.put("hibernate.connection.charSet", hibernateConnectionCharSet);
        properties.put("hibernate.connection.characterEncoding", hibernateConnectionCharacterEncoding);
        properties.put("hibernate.connection.useUnicode", hibernateConnectionUseUnicode);

        return properties;
    }

    @Bean
    public HibernateTransactionManager getHibernateTransactionManager() {

        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(getLocalSessionFactory().getObject());

        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor getPersistenceExceptionTranslationPostProcessor() {

        PersistenceExceptionTranslationPostProcessor translator = new PersistenceExceptionTranslationPostProcessor();

        return translator;
    }

    /* Getters and setters */
    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHibernateDialect() {
        return hibernateDialect;
    }

    public void setHibernateDialect(String hibernateDialect) {
        this.hibernateDialect = hibernateDialect;
    }

    public String getHibernateShowSql() {
        return hibernateShowSql;
    }

    public void setHibernateShowSql(String hibernateShowSql) {
        this.hibernateShowSql = hibernateShowSql;
    }

    public String getHibernateHbm2ddlAuto() {
        return hibernateHbm2ddlAuto;
    }

    public void setHibernateHbm2ddlAuto(String hibernateHbm2ddlAuto) {
        this.hibernateHbm2ddlAuto = hibernateHbm2ddlAuto;
    }
}
