package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configuring the DAOs
 */
@Configuration
@Import({HibernateConfig.class})
@ComponentScan("databasedao.hibernate")
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class DaoConfig {

    @Autowired
    private HibernateConfig hibernateConfig;
}
