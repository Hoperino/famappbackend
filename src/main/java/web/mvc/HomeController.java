package web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller to serve index page
 */
@Controller(value = "/")
public class HomeController {

    public HomeController(){
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String printWelcome(ModelMap model, HttpServletRequest request) {
        return "redirect:/resources/html/index.html";
    }
}
