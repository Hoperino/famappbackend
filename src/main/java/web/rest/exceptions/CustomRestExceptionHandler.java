package web.rest.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Exception handling for rest
 */
@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        JsonError apiError =
                new JsonError(errors, ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(
                ex, apiError, headers, apiError.getStatus(), request);
    }

    @ExceptionHandler(IdNotFoundException.class)
    public ResponseEntity<Object> handleIdNotValid(
            IdNotFoundException ex,
            WebRequest request
    ) {
        JsonError apiError = new JsonError(ex.getMessage(), ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex,
            WebRequest request) {
        String error =
                ex.getName() + " should be of type " + ex.getRequiredType().getName();

        JsonError apiError =
                new JsonError(error, ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler(IdFieldInvalidException.class)
    public ResponseEntity<Object> handleIdFieldInvalidException(
            IdFieldInvalidException ex,
            WebRequest request
    ) {
        JsonError apiError = new JsonError(ex.getMessage(), ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler(QueryParamsInvalidException.class)
    public ResponseEntity<Object> handleQueryParamsInvalidException(
            QueryParamsInvalidException ex,
            WebRequest request
    ) {
        JsonError apiError = new JsonError(ex.getMessage(), ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

        JsonError apiError = new JsonError(error, ex.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }
}
