package web.rest.exceptions;

import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Class used to send error messages in JSON
 */
public class JsonError {
    private HttpStatus status;
    private String message;
    private List<String> errors;

    public JsonError(List<String> errors, String message, HttpStatus status) {
        super();
        this.errors = errors;
        this.status = status;
        this.message = message;
    }

    public JsonError(String error, String message, HttpStatus status) {
        super();
        this.errors = Arrays.asList(error);
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
