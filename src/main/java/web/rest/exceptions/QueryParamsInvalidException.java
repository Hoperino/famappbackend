package web.rest.exceptions;

/**
 * Thrown when query params from a request are invalid
 */
public class QueryParamsInvalidException extends RuntimeException {
    public QueryParamsInvalidException(String message) {
        super(message);
    }
}
