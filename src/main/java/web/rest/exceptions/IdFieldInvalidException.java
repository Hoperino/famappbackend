package web.rest.exceptions;

/**
 * Throws this exception when Id is invalid
 */
public class IdFieldInvalidException extends RuntimeException {
    public IdFieldInvalidException(String message) {
        super(message);
    }
}
