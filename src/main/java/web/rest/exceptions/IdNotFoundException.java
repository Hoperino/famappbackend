package web.rest.exceptions;

/**
 * This exception is thrown when owner ID is not found.
 */
public class IdNotFoundException extends RuntimeException {

    public IdNotFoundException(String exception) {
        super(exception);
    }
}
