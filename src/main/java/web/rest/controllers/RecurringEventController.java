package web.rest.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.RecurringEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.interfaces.RecurringEventService;
import web.rest.exceptions.IdFieldInvalidException;
import web.rest.utility.RestUriConstants;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Recurring event Rest controller
 */
@RestController("/rest/event/*")
public class RecurringEventController {

    @Autowired
    private RecurringEventService recurringEventService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @RequestMapping(value = RestUriConstants.GET_RECURRING_EVENT, method = RequestMethod.GET)
    public RecurringEvent getRecurringEvent(@PathVariable("id") long eventId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(eventId, RecurringEvent.class);

        return recurringEventService.findRecurringEventById(eventId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_RECURRING_EVENTS, method = RequestMethod.GET)
    public List<RecurringEvent> getALlRecurringEvents(){
        return recurringEventService.getAllRecurringEvent();
    }

    @RequestMapping(value = RestUriConstants.CREATE_RECURRING_EVENT, method = RequestMethod.POST)
    public ResponseEntity<JsonNode> createRecurringEvent(@RequestBody RecurringEvent recurringEvent){
        /*
         * Check if the id is different than null (Id should be null when creating new Recurring event, since
         * the database creates one)
         */
        if (recurringEvent.getId() != null) {
            throw new IdFieldInvalidException("A value for Id was found! Please, provide a value of null!");
        }
        Long id = recurringEventService.createRecurringEvent(recurringEvent);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        jsonNode.put("recurringEvent_id", id);

        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = RestUriConstants.UPDATE_RECURRING_EVENT, method = RequestMethod.POST)
    public void updateRecurringEvent(@RequestBody RecurringEvent recurringEvent){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(recurringEvent.getId(), RecurringEvent.class);

        /*
        * Refresh from DB the relationships. The request body excludes the documents and payments lists,
        * hence we have to attach them before saving, otherwise they will be overwritten.
        */
        recurringEvent.setPaymentSet(recurringEventService.getPaymentsForRecurringEventId(recurringEvent.getId()));
        recurringEvent.setDocumentSet(recurringEventService.getDocumentsForRecurringEventId(recurringEvent.getId()));

        recurringEventService.updateRecurringEvent(recurringEvent);
    }

    @RequestMapping(value = RestUriConstants.DELETE_RECURRING_EVENT, method = RequestMethod.GET)
    public ResponseEntity<JsonNode> deleteRecurringEvent(@PathVariable("id") long eventId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(eventId, RecurringEvent.class);
        recurringEventService.removeRecurringEvent(eventId);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);

        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }
}
