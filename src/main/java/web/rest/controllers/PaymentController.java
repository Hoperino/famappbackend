package web.rest.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.Insurance;
import model.Payment;
import model.RecurringEvent;
import model.property.Property;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.interfaces.PaymentService;
import web.rest.exceptions.IdFieldInvalidException;
import web.rest.utility.RestUriConstants;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Payment Rest Controller
 */
@RestController("/rest/payment/*")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @RequestMapping(value = RestUriConstants.GET_PAYMENT, method = RequestMethod.GET)
    public Payment getPayment(@PathVariable("id") long paymentId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(paymentId, Payment.class);

        return paymentService.findPaymentById(paymentId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_PAYMENTS, method = RequestMethod.GET)
    public List<Payment> getAllPayments(){
        return paymentService.getAllPayments();
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_PAYMENTS_FOR_PROPERTY, method = RequestMethod.GET)
    public List<Payment> getAllPaymentsForProperty(@PathVariable(value = "id") long propertyId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(propertyId, Property.class);

        return paymentService.getAllPaymentsForProperty(propertyId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_PAYMENTS_FOR_INSURANCE, method = RequestMethod.GET)
    public List<Payment> getAllPaymentsForInsurance(@PathVariable(value = "id") long insuranceId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(insuranceId, Insurance.class);

        return paymentService.getAllPaymentsForInsurance(insuranceId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_PAYMENTS_FOR_RECURRING_EVENT, method = RequestMethod.GET)
    public List<Payment> getAllPaymentsForRecurringEvent(@PathVariable(value = "id") long eventId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(eventId, RecurringEvent.class);
        return paymentService.getAllPaymentsForRecurringEvent(eventId);
    }

    @RequestMapping(value = RestUriConstants.CREATE_PAYMENT, method = RequestMethod.POST)
    public ResponseEntity<JsonNode> createPayment(
            @RequestBody Payment payment,
            @RequestParam("assetId") long assetId,
            @RequestParam("assetType") AssetType assetType){
        /*
         * Check if the id is different than null (Id should be null when creating new Payment, since
         * the database creates one)
         */
        if (payment.getId() != null) {
            throw new IdFieldInvalidException("A value for Id was found! Please, provide a value of null!");
        }
        utilityRestMethods.validateQueryParams(assetType, assetId);
        Long id = paymentService.createPayment(payment, assetId, assetType);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        jsonNode.put("payment_Id", id);

        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = RestUriConstants.UPDATE_PAYMENT, method = RequestMethod.POST)
    public void updatePayment(@RequestBody Payment payment){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(payment.getId(), Payment.class);
        paymentService.updatePayment(payment);
    }

    @RequestMapping(value = RestUriConstants.DELETE_PAYMENT, method = RequestMethod.GET)
    public ResponseEntity<JsonNode> deletePayment(
            @PathVariable("id") long paymentId,
            @RequestParam("assetId") long assetId,
            @RequestParam("assetType") AssetType assetType){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(paymentId, Payment.class);
        utilityRestMethods.validateQueryParams(assetType, assetId);
        paymentService.removePayment(paymentId, assetId, assetType);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);

        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }
}
