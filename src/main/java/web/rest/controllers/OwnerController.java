package web.rest.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.interfaces.OwnerService;
import web.rest.exceptions.IdFieldInvalidException;
import web.rest.utility.RestUriConstants;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Rest controller for handling owner requests
 */
@RestController("/rest/owner/*")
public class OwnerController {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @RequestMapping(value = RestUriConstants.GET_OWNER, method = RequestMethod.GET)
    public Owner getOwner(@PathVariable("id") long ownerId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(ownerId, Owner.class);
        return ownerService.findOwnerById(ownerId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_OWNERS, method = RequestMethod.GET)
    public List<Owner> getAllOwners(){
        return ownerService.getAllOwners();
    }

    @RequestMapping(value = RestUriConstants.CREATE_OWNER, method = RequestMethod.POST)
    public ResponseEntity<JsonNode> createOwner(@RequestBody Owner owner){
        /*
         * Check if the id is different than null (Id should be null when creating new Owner, since
         * the database creates one)
         */
        if (owner.getId() != null) {
            throw new IdFieldInvalidException("A value for Id was found! Please, provide a value of null!");
        }
        Long id = ownerService.createOwner(owner);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        jsonNode.put("owner_id",id);

        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = RestUriConstants.UPDATE_OWNER, method = RequestMethod.POST)
    public void updateOwner(@RequestBody Owner owner){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(owner.getId(), Owner.class);
        /*
         * Refresh from DB the relationships. The request body excludes the insurance and property lists,
         * hence we have to attach them before saving, otherwise they will be overwritten.
         */
        owner.setInsuranceSet(ownerService.getInsurancesForOwnerId(owner.getId()));
        owner.setPropertySet(ownerService.getPropertiesForOwnerId(owner.getId()));

        ownerService.updateOwner(owner);
    }

    @RequestMapping(value = RestUriConstants.DELETE_OWNER, method = RequestMethod.GET)
    public ResponseEntity<JsonNode> deleteOwner(@PathVariable("id") long ownerId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(ownerId, Owner.class);
        ownerService.removeOwner(ownerId);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }
}
