package web.rest.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.interfaces.PropertyService;
import web.rest.exceptions.IdFieldInvalidException;
import web.rest.utility.RestUriConstants;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Property Rest Controller
 */
@RestController("/rest/property/*")
public class PropertyController {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @RequestMapping(value = RestUriConstants.GET_PROPERTY, method = RequestMethod.GET)
    public Property getProperty(@PathVariable("id") long propertyId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(propertyId, Property.class);

        return propertyService.findProperty(propertyId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_PROPERTIES, method = RequestMethod.GET)
    public List<Property> getAllProperties(){
        return propertyService.getAllProperties();
    }

    @RequestMapping(value = RestUriConstants.CREATE_PROPERTY, method = RequestMethod.POST)
    public ResponseEntity<JsonNode> createProperty(@RequestBody Property property){
        /*
         * Check if the id is different than null (Id should be null when creating new Property, since
         * the database creates one)
         */
        if (property.getId() != null) {
            throw new IdFieldInvalidException("A value for Id was found! Please, provide a value of null!");
        }
        /*
         * Cast the Property to appropriate instance
         */
        Long id;
        if (property instanceof Stock){
            Stock propertyStock = (Stock) property;
            id = propertyService.createProperty(propertyStock);
        }else if (property instanceof RealEstate){
            RealEstate propertyRealEstate = (RealEstate) property;
            id = propertyService.createProperty(propertyRealEstate);
        }else {
            Vehicle propertyVehicle = (Vehicle) property;
            id = propertyService.createProperty(propertyVehicle);
        }
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        jsonNode.put("property_Id",id);

        return new ResponseEntity<>(jsonNode, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = RestUriConstants.UPDATE_PROPERTY, method = RequestMethod.POST)
    public void updateProperty(@RequestBody Property property){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(property.getId(), Property.class);

        /*
        * Refresh from DB the relationships. The request body excludes the documents and payments lists,
        * hence we have to attach them before saving, otherwise they will be overwritten.
        */
        if (property instanceof Stock){
            Stock propertyStock = (Stock) property;
            propertyStock.setPaymentSet(propertyService.getPaymentsForPropertyId(propertyStock.getId()));
            propertyStock.setDocumentSet(propertyService.getDocumentsForPropertyId(propertyStock.getId()));
            propertyService.updateProperty(propertyStock);
        }else if (property instanceof RealEstate){
            RealEstate propertyRealEstate = (RealEstate) property;
            propertyRealEstate.setPaymentSet(propertyService.getPaymentsForPropertyId(propertyRealEstate.getId()));
            propertyRealEstate.setDocumentSet(propertyService.getDocumentsForPropertyId(propertyRealEstate.getId()));
            propertyService.updateProperty(propertyRealEstate);
        }else {
            Vehicle propertyVehicle = (Vehicle) property;
            propertyVehicle.setPaymentSet(propertyService.getPaymentsForPropertyId(propertyVehicle.getId()));
            propertyVehicle.setDocumentSet(propertyService.getDocumentsForPropertyId(propertyVehicle.getId()));
            propertyService.updateProperty(propertyVehicle);
        }
    }

    @RequestMapping(value = RestUriConstants.DELETE_PROPERTY, method = RequestMethod.GET)
    public ResponseEntity<JsonNode> deleteProperty(@PathVariable("id") long propertyId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(propertyId, Property.class);
        propertyService.removeProperty(propertyId);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);

        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }
}
