package web.rest.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.Insurance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.interfaces.InsuranceService;
import web.rest.exceptions.IdFieldInvalidException;
import web.rest.utility.RestUriConstants;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Insurance Rest controller
 */
@RestController("/rest/insurance/*")
public class InsuranceController {

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @RequestMapping(value = RestUriConstants.GET_INSURANCE, method = RequestMethod.GET)
    public Insurance getInsurance(@PathVariable("id") long insuranceId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(insuranceId, Insurance.class);
        return insuranceService.findInsuranceById(insuranceId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_INSURANCES, method = RequestMethod.GET)
    public List<Insurance> getAllInsurances(){
        return insuranceService.getAllInsurances();
    }

    @RequestMapping(value = RestUriConstants.CREATE_INSURANCE, method = RequestMethod.POST)
    public ResponseEntity<JsonNode> createInsurance(@RequestBody Insurance insurance){
        /*
         * Check if the id is different than null (Id should be null when creating new Insurance, since
         * the database creates one)
         */
        if (insurance.getId() != null) {
            throw new IdFieldInvalidException("A value for Id was found! Please, provide a value of null!");
        }
        Long id = insuranceService.createInsurance(insurance);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        jsonNode.put("insurance_id", id);
        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = RestUriConstants.UPDATE_INSURANCE, method = RequestMethod.POST)
    public void updateInsurance(@RequestBody Insurance insurance){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(insurance.getId(), Insurance.class);

        /*
         * Refresh from DB the relationships. The request body excludes the documents and payments lists,
         * hence we have to attach them before saving, otherwise they will be overwritten.
         */
        insurance.setPaymentSet(insuranceService.getPaymentsForInsuranceId(insurance.getId()));
        insurance.setDocumentSet(insuranceService.getDocumentsForInsuranceId(insurance.getId()));

        insuranceService.updateInsurance(insurance);
    }

    @RequestMapping(value = RestUriConstants.DELETE_INSURANCE, method = RequestMethod.GET)
    public ResponseEntity<JsonNode> deleteInsurance(@PathVariable("id") long insuranceId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(insuranceId, Insurance.class);
        insuranceService.removeInsurance(insuranceId);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        return new ResponseEntity<>(jsonNode,new HttpHeaders(), HttpStatus.OK);
    }
}
