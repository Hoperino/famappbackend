package web.rest.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import model.Document;
import model.Insurance;
import model.RecurringEvent;
import model.property.Property;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.interfaces.DocumentService;
import web.rest.exceptions.IdFieldInvalidException;
import web.rest.utility.RestUriConstants;
import web.rest.utility.UtilityRestMethods;

import java.util.List;

/**
 * Document Rest Controller
 */
@RestController("/rest/document/*")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @RequestMapping(value = RestUriConstants.GET_DOCUMENT, method = RequestMethod.GET)
    public Document getDocument(@PathVariable("id") long documentId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(documentId, Document.class);

        return documentService.findDocumentById(documentId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_DOCUMENTS, method = RequestMethod.GET)
    public List<Document> getAllDocuments(){
        return documentService.getAllDocuments();
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_DOCUMENTS_FOR_PROPERTY, method = RequestMethod.GET)
    public List<Document> getAllDocumentsForProperty(@PathVariable(value = "id") long propertyId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(propertyId, Property.class);

        return documentService.getAllDocumentsForProperty(propertyId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_DOCUMENTS_FOR_INSURANCE, method = RequestMethod.GET)
    public List<Document> getAllDocumentsForInsurance(@PathVariable(value = "id") long insuranceId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(insuranceId, Insurance.class);
        return documentService.getAllDocumentsForInsurance(insuranceId);
    }

    @RequestMapping(value = RestUriConstants.GET_ALL_DOCUMENTS_FOR_RECURRING_EVENT, method = RequestMethod.GET)
    public List<Document> getAllDocumentsForRecurringEvent(@PathVariable(value = "id") long eventId){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(eventId, RecurringEvent.class);
        return documentService.getAllDocumentsForRecurringEvent(eventId);
    }

    @RequestMapping(value = RestUriConstants.CREATE_DOCUMENT, method = RequestMethod.POST)
    public ResponseEntity<JsonNode> createDocument(
            @RequestBody Document document,
            @RequestParam("assetId") long assetId,
            @RequestParam("assetType") AssetType assetType){
        /*
         * Check if the id is different than null (Id should be null when creating new Document, since
         * the database creates one)
         */
        if (document.getId() != null) {
            throw new IdFieldInvalidException("A value for Id was found! Please, provide a value of null!");
        }
        utilityRestMethods.validateQueryParams(assetType, assetId);
        Long id = documentService.createDocument(document, assetId, assetType);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);
        jsonNode.put("document_id", id);

        return new ResponseEntity<>(jsonNode, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = RestUriConstants.UPDATE_DOCUMENT, method = RequestMethod.POST)
    public void updateDocument(@RequestBody Document document){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(document.getId(), Document.class);
        documentService.updateDocument(document);
    }

    @RequestMapping(value = RestUriConstants.DELETE_DOCUMENT, method = RequestMethod.GET)
    public ResponseEntity<JsonNode> deleteDocument(
            @PathVariable("id") long documentId,
            @RequestParam("assetId") long assetId,
            @RequestParam("assetType") AssetType assetType){
        /*
         * Check if the id is valid
         */
        utilityRestMethods.validateIdExists(documentId, Document.class);
        utilityRestMethods.validateQueryParams(assetType, assetId);
        documentService.removeDocument(documentId, assetId, assetType);
        ObjectNode jsonNode = new ObjectNode(JsonNodeFactory.instance);

        return new ResponseEntity<>(jsonNode, new HttpHeaders(), HttpStatus.OK);
    }
}
