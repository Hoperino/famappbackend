package web.rest.utility;

import model.*;
import model.property.Property;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import service.interfaces.*;
import web.rest.exceptions.IdNotFoundException;
import web.rest.exceptions.QueryParamsInvalidException;

/**
 * Define methods that are used by REST and are static
 */
public class UtilityRestMethods {

    private final static UtilityRestMethods instance = new UtilityRestMethods();

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private RecurringEventService recurringEventService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private OwnerService ownerService;

    private UtilityRestMethods() {}

    public static UtilityRestMethods getInstance() {
        return instance;
    }

    /**
     * Check if query params are null or invalid
     * @throws QueryParamsInvalidException
     */
    public void validateQueryParams(AssetType assetType, long assetId) {

        /* Check if AssetType is null */
        if (assetType == null) {
            throw new QueryParamsInvalidException("Query param assetType is invalid, assetType is null. " +
                    "It should be \"REAL_ESTATE\" or \"STOCK\" or \"VEHICLE\" or \"INSURANCE\" or \"RECURRING_EVENT\".");
        }
        /* Check if assetId is a valid Id for the given assetType record */
        switch (assetType) {
            case STOCK:
            case VEHICLE:
            case REAL_ESTATE:
                Property checkProperty = propertyService.findProperty(assetId);
                if (checkProperty == null) {
                    throw new  QueryParamsInvalidException("Query param assetId is invalid or no matching record was found with it." +
                            "It should be a valid, positive integer");
                }break;
            case INSURANCE:
                Insurance checkInsurance = insuranceService.findInsuranceById(assetId);
                if (checkInsurance == null) {
                    throw new  QueryParamsInvalidException("Query param assetId is invalid or no matching record was found with it." +
                            "It should be a valid, positive integer");
                }break;
            case RECURRING_EVENT:
                RecurringEvent checkEvent = recurringEventService.findRecurringEventById(assetId);
                if (checkEvent == null) {
                    throw new  QueryParamsInvalidException("Query param assetId is invalid or no matching record was found with it." +
                            "It should be a valid, positive integer");
                }break;
            default:
                throw new QueryParamsInvalidException("Query param assetType is invalid, assetType is null. " +
                        "It should be \"REAL_ESTATE\" or \"STOCK\" or \"VEHICLE\" or \"INSURANCE\" or \"RECURRING_EVENT\".");
        }
    }

    /**
     * Check if id is valid in the db
     * @throws IdNotFoundException
     */
    public void validateIdExists(long id, Class typeClass) {
        System.out.println(typeClass.getSimpleName());
        switch (typeClass.getSimpleName()) {
            case "Insurance":
                Insurance insurance = insuranceService.findInsuranceById(id);
                if (insurance == null) {
                    throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() + " in the System!");
                }break;
            case "RecurringEvent":
                RecurringEvent recurringEvent = recurringEventService.findRecurringEventById(id);
                if (recurringEvent == null) {
                    throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() +" in the System!");
                }break;
            case "Property":
            case "Vehicle":
            case "RealEstate":
            case "Stock":
                Property property = propertyService.findProperty(id);
                if (property == null) {
                    throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() + " in the System!");
                }break;
            case "Payment":
                Payment payment = paymentService.findPaymentById(id);
                if (payment == null) {
                    throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() + " in the System!");
                }break;
            case "Document":
                Document document = documentService.findDocumentById(id);
                if (document == null) {
                    throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() + " in the System!");
                }break;
            case "Owner":
                Owner owner = ownerService.findOwnerById(id);
                if (owner == null) {
                    throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() +" in the System!");
                }break;
            default:
                throw new IdNotFoundException("Id: " + id + " was not found for " + typeClass.getSimpleName() + " in the System!");
        }
    }
}
