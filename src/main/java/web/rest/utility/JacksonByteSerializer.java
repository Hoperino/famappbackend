package web.rest.utility;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Base64;

/**
 * Serializes byte array into base64 encoding
 */
public class JacksonByteSerializer extends StdSerializer<byte[]> {

    public JacksonByteSerializer() {
        super(byte[].class);
    }

    @Override
    public void serialize(byte[] value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(new String(Base64.getEncoder().encode(value)));
    }
}
