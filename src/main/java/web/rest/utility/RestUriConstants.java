package web.rest.utility;

/**
 * Define URI for restful service
 */
public class RestUriConstants {

    /**
     * Owner URI
     *
     * URI: "servername"/rest/
     * */
    public static final String GET_OWNER = "/owner/{id}";
    public static final String GET_ALL_OWNERS = "/owner/all";
    public static final String CREATE_OWNER = "/owner/create";
    public static final String UPDATE_OWNER = "/owner/update";
    public static final String DELETE_OWNER = "/owner/delete/{id}";

//    /**
//     * Avatar URI
//     *
//     * URI: "servername"/rest/
//     * */
//    public static final String GET_AVATAR = "/avatar/{id}";
//    public static final String GET_AVATARS_FOR_MEMBER = "/avatar/formember/{memberId}";
//    public static final String UPDATE_AVATAR = "/avatar/update";
//    public static final String CREATE_AVATAR_FOR_MEMBER = "/avatar/create/{memberId}";
//    public static final String DELETE_AVATAR = "/avatar/delete/{id}";

    /**
     * Property URI
     *
     * URI: "servername"/rest/
     * */
    public static final String GET_PROPERTY = "/property/{id}";
    public static final String GET_ALL_PROPERTIES = "/property/all";
    public static final String CREATE_PROPERTY = "/property/create";
    public static final String UPDATE_PROPERTY = "/property/update";
    public static final String DELETE_PROPERTY = "/property/delete/{id}";

    /**
     * Insurance URI
     *
     * URI: "servername"/rest/
     * */
    public static final String GET_INSURANCE = "/insurance/{id}";
    public static final String GET_ALL_INSURANCES = "/insurance/all";
    public static final String CREATE_INSURANCE = "/insurance/create";
    public static final String UPDATE_INSURANCE = "/insurance/update";
    public static final String DELETE_INSURANCE = "/insurance/delete/{id}";

    /**
     * Recurring Event URI
     *
     * URI: "servername"/rest/
     * */
    public static final String GET_RECURRING_EVENT = "/event/{id}";
    public static final String GET_ALL_RECURRING_EVENTS = "/event/all";
    public static final String CREATE_RECURRING_EVENT = "/event/create";
    public static final String UPDATE_RECURRING_EVENT = "/event/update";
    public static final String DELETE_RECURRING_EVENT = "/event/delete/{id}";

    /**
     * Document URI
     *
     * URI: "servername"/rest/
     * */
    public static final String GET_DOCUMENT = "/document/{id}";
    public static final String GET_ALL_DOCUMENTS = "/document/all";
    public static final String GET_ALL_DOCUMENTS_FOR_PROPERTY = "/document/property/{id}";
    public static final String GET_ALL_DOCUMENTS_FOR_INSURANCE = "/document/insurance/{id}";
    public static final String GET_ALL_DOCUMENTS_FOR_RECURRING_EVENT = "document/event/{id}";
    public static final String CREATE_DOCUMENT = "/document/create";
    public static final String UPDATE_DOCUMENT = "/document/update";
    public static final String DELETE_DOCUMENT = "/document/delete/{id}";

    /**
     * Payment URI
     *
     * URI: "servername"/rest/
     * */
    public static final String GET_PAYMENT = "/payment/{id}";
    public static final String GET_ALL_PAYMENTS = "/payment/all";
    public static final String GET_ALL_PAYMENTS_FOR_PROPERTY = "/payment/property/{id}";
    public static final String GET_ALL_PAYMENTS_FOR_INSURANCE = "/payment/insurance/{id}";
    public static final String GET_ALL_PAYMENTS_FOR_RECURRING_EVENT = "payment/event/{id}";
    public static final String CREATE_PAYMENT = "/payment/create";
    public static final String UPDATE_PAYMENT = "/payment/update";
    public static final String DELETE_PAYMENT = "/payment/delete/{id}";

}
