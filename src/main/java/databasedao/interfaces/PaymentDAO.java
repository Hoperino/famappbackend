package databasedao.interfaces;

import model.Payment;

/**
 * Payment DAO object
 */
public interface PaymentDAO extends GenericDAO<Payment, Long> {
}
