package databasedao.interfaces;

import model.Avatar;

/**
 * Avatar DAO object
 */
public interface AvatarDAO extends GenericDAO<Avatar, Long> {
}
