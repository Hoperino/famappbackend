package databasedao.interfaces;

import model.Insurance;

/**
 * Insurance DAO object
 */
public interface InsuranceDAO extends GenericDAO<Insurance, Long> {
}
