package databasedao.interfaces;

import java.io.Serializable;
import java.util.Set;

/**
 * Generic CRUD DAO structure that is used as template by other DAOs
 */
public interface GenericDAO<T,PK extends Serializable> {

    /** CREATE */
    PK add(T o);

    /** UPDATE */
    void update(T transientObject);

    /** DELETE */
    void delete(T persistentObject);

    /** GET BY ID */
    T findById(PK id);

    /** GET ALL ENTRIES */
    Set<T> getAll();
}
