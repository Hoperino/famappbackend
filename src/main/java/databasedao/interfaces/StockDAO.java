package databasedao.interfaces;

import model.property.Stock;

/**
 * Stock DAO object
 */
public interface StockDAO extends GenericDAO<Stock, Long> {
}
