package databasedao.interfaces;

import model.property.Property;

/**
 * Property DAO used for specific operations and not for managing the table
 */
public interface PropertyDAO {

    Property findProperty(Long propertyId);

    void updateProperty(Property property);

    void removeProperty(Long propertyId);
}
