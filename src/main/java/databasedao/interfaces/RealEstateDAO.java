package databasedao.interfaces;

import model.property.RealEstate;

/**
 * Real Estate DAO object
 */
public interface RealEstateDAO extends GenericDAO<RealEstate, Long> {
}
