package databasedao.interfaces;

import model.RecurringEvent;

/**
 * Recurring Event DAO object
 */
public interface RecurringEventDAO extends GenericDAO<RecurringEvent, Long> {
}
