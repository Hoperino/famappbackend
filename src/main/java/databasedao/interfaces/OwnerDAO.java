package databasedao.interfaces;

import model.Owner;

/**
 * Owner DAO object
 */
public interface OwnerDAO extends GenericDAO<Owner,Long> {
}
