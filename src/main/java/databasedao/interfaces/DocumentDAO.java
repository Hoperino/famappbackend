package databasedao.interfaces;

import model.Document;

/**
 * Document DAO object
 */
public interface DocumentDAO extends GenericDAO<Document, Long> {
}
