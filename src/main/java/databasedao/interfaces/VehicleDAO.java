package databasedao.interfaces;

import model.property.Vehicle;

/**
 * Vehicle DAO object
 */
public interface VehicleDAO extends GenericDAO<Vehicle, Long> {
}
