package databasedao.hibernate;

import model.property.Vehicle;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.VehicleDAO;

/**
 * Vehicle DAO hibernate implementation
 */
@Repository("vehicleDao")
public class VehicleDAOHibernateImpl extends GenericDAOHibernateImpl<Vehicle, Long> implements VehicleDAO {
}
