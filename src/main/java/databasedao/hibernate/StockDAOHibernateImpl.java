package databasedao.hibernate;

import model.property.Stock;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.StockDAO;

/**
 * Stock DAO hibernate implementation
 */
@Repository("stockDao")
public class StockDAOHibernateImpl extends GenericDAOHibernateImpl<Stock, Long> implements StockDAO {
}
