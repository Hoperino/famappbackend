package databasedao.hibernate;

import model.Payment;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.PaymentDAO;

/**
 * Payment DAO hibernate implementation
 */
@Repository("paymentDao")
public class PaymentDAOImpl extends GenericDAOHibernateImpl<Payment, Long> implements PaymentDAO {
}
