package databasedao.hibernate;

import databasedao.interfaces.PropertyDAO;
import model.property.Property;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Property DAO hibernate implementation
 */
@Repository("propertyDao")
public class PropertyDAOImpl implements PropertyDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Property findProperty(Long propertyId) {
        return sessionFactory.getCurrentSession().get(Property.class, propertyId);
    }

    @Override
    public void updateProperty(Property property) {
        sessionFactory.getCurrentSession().merge(property);
    }

    @Override
    public void removeProperty(Long propertyId) {
        Property property = sessionFactory.getCurrentSession().get(Property.class, propertyId);
        sessionFactory.getCurrentSession().delete(property);
    }
}
