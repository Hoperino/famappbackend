package databasedao.hibernate;

import model.property.RealEstate;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.RealEstateDAO;

/**
 * Real Estate hibernate implementation
 */
@Repository("realEstateDao")
public class RealEstateHibernateImpl extends GenericDAOHibernateImpl<RealEstate, Long> implements RealEstateDAO {
}
