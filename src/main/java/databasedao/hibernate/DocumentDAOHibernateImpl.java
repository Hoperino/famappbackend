package databasedao.hibernate;

import model.Document;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.DocumentDAO;

/**
 * Document DAO hibernate implementation
 */
@Repository("documentDao")
public class DocumentDAOHibernateImpl extends GenericDAOHibernateImpl<Document, Long> implements DocumentDAO {
}
