package databasedao.hibernate;

import model.Avatar;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.AvatarDAO;

/**
 * Avatar DAO hibernate implementation
 */
@Repository("avatarDao")
public class AvatarDAOHibernateImpl extends GenericDAOHibernateImpl<Avatar, Long> implements AvatarDAO {
}
