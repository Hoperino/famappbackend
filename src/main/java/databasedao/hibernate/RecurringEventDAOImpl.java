package databasedao.hibernate;

import model.RecurringEvent;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.RecurringEventDAO;

/**
 * Recurring Event DAO hibernate implementation
 */
@Repository("recurringEventDao")
public class RecurringEventDAOImpl extends GenericDAOHibernateImpl<RecurringEvent, Long> implements RecurringEventDAO {
}
