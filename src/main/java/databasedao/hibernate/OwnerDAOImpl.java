package databasedao.hibernate;

import databasedao.interfaces.*;
import model.Owner;
import org.springframework.stereotype.Repository;

/**
 * Owner DAO hibernate implementation
 */
@Repository("ownerDao")
public class OwnerDAOImpl extends GenericDAOHibernateImpl<Owner, Long> implements OwnerDAO {
}
