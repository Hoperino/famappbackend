package databasedao.hibernate;

import model.Insurance;
import org.springframework.stereotype.Repository;
import databasedao.interfaces.InsuranceDAO;

/**
 * Insurance DAO hibernate implementation
 */
@Repository("insuranceDao")
public class InsuranceDAOImpl extends GenericDAOHibernateImpl<Insurance, Long> implements InsuranceDAO {
}
