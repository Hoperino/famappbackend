package databasedao.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import databasedao.interfaces.GenericDAO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of Generic DAO for Hibernate
 */
@Transactional(propagation= Propagation.REQUIRED, readOnly=false, isolation = Isolation.SERIALIZABLE)
public class GenericDAOHibernateImpl<T,PK extends Serializable> implements GenericDAO<T, PK> {

    private Class<T> type;

    @Autowired(required = true)
    private SessionFactory factory;

    public GenericDAOHibernateImpl(){
        type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public PK add(T o) {
        return (PK) getSession().save(o);
    }

    @Override
    public void update(T transientObject) {
        getSession().saveOrUpdate(transientObject);
    }

    @Override
    public void delete(T persistentObject) {
        getSession().remove(persistentObject);
    }

    /**
     * @throws NullPointerException
     * */
    @Override
    public T findById(PK id) {
        return getSession().get(type,id);
    }

    @Override
    public Set<T> getAll() {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(type);
        Root<T> variableRoot = query.from(type);
        query.select(variableRoot);
        List<T> tempList = getSession().createQuery(query).getResultList();
        return new HashSet<>(tempList);
    }

    /* Getters and Setters */

    public SessionFactory getFactory() {
        return factory;
    }

    public void setFactory(SessionFactory factory) {
        this.factory = factory;
    }

    /* Other methods
     * Default package-private for DAO implementations */
    Session getSession(){
        return factory.getCurrentSession();
    }
}
