package service.hibernate;

import databasedao.interfaces.InsuranceDAO;
import model.Document;
import model.Insurance;
import model.Payment;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.DocumentService;
import service.interfaces.InsuranceService;
import service.interfaces.PaymentService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Insurance service hibernate implementation
 */
@Service("insuranceService")
@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class InsuranceServiceImpl implements InsuranceService {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Autowired
    private PaymentService paymentService;

    @Override
    public Long createInsurance(Insurance insurance) {
        return insuranceDAO.add(insurance);
    }

    @Override
    public Insurance findInsuranceById(Long insuranceId) {
        return insuranceDAO.findById(insuranceId);
    }

    @Override
    public void updateInsurance(Insurance insurance) {
        insuranceDAO.update(insurance);
    }

    @Override
    public List<Insurance> getAllInsurances() {
        return new ArrayList<>(insuranceDAO.getAll());
    }

    @Override
    public void removeInsurance(Long insuranceId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        List<Payment> paymentList = new ArrayList<>(
                paymentService.getAllPaymentsForInsurance(insuranceId));
        List<Document> documentList = new ArrayList<>(
                documentService.getAllDocumentsForInsurance(insuranceId));
        for (Payment x: paymentList) {
            paymentService.removePayment(x.getId(), insuranceId, AssetType.INSURANCE);
        }
        for (Document x: documentList) {
            documentService.removeDocument(x.getId(), insuranceId, AssetType.INSURANCE);
        }
        insuranceDAO.delete(insurance);
    }

    @Override
    public Set<Document> getDocumentsForInsuranceId(Long insuranceId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        return new HashSet<>(insurance.getDocumentSet());
    }

    @Override
    public Set<Payment> getPaymentsForInsuranceId(Long insuranceId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        return new HashSet<>(insurance.getPaymentSet());
    }
}
