package service.hibernate;

import databasedao.interfaces.*;
import model.Insurance;
import model.Owner;
import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.InsuranceService;
import service.interfaces.OwnerService;
import service.interfaces.PropertyService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Owner service hibernate implementation
 */
@Service("ownerService")
@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnerDAO ownerDAO;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Autowired
    private RealEstateDAO realEstateDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private VehicleDAO vehicleDAO;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private PropertyService propertyService;


    @Override
    public Long createOwner(Owner owner) {
        return ownerDAO.add(owner);
    }

    @Override
    public void updateOwner(Owner owner) {
        ownerDAO.update(owner);
    }

    @Override
    public Owner findOwnerById(Long ownerId) {
        return ownerDAO.findById(ownerId);
    }

    @Override
    public List<Owner> getAllOwners() {
        return new ArrayList<>(ownerDAO.getAll());
    }

    @Override
    public void removeOwner(Long ownerId) {
        Owner owner = ownerDAO.findById(ownerId);
        List<Insurance> insuranceList = new LinkedList<>(
                insuranceDAO.getAll())
                .stream()
                .filter((insurance -> (long) insurance.getOwner().getId() == ownerId))
                .collect((Collectors.toList()));
        List<RealEstate> realEstatesList = new LinkedList<>(
                realEstateDAO.getAll())
                .stream()
                .filter((realEstate -> (long) realEstate.getOwner().getId() == ownerId))
                .collect((Collectors.toList()));
        List<Stock> stocksList = new LinkedList<>(
                stockDAO.getAll())
                .stream()
                .filter((stock -> (long) stock.getOwner().getId() == ownerId))
                .collect((Collectors.toList()));
        List<Vehicle> vehiclesList = new LinkedList<>(
                vehicleDAO.getAll())
                .stream()
                .filter((vehicle -> (long) vehicle.getOwner().getId() == ownerId))
                .collect((Collectors.toList()));
        for (RealEstate x: realEstatesList) {
            propertyService.removeProperty(x.getId());
        }
        for (Stock x: stocksList) {
            propertyService.removeProperty(x.getId());
        }
        for (Vehicle x: vehiclesList) {
            propertyService.removeProperty(x.getId());
        }
        for (Insurance x: insuranceList
                ) {
            insuranceService.removeInsurance(x.getId());
        }
        ownerDAO.delete(owner);
    }

    @Override
    public Set<Insurance> getInsurancesForOwnerId(Long ownerId) {
        Owner owner = ownerDAO.findById(ownerId);
        return new HashSet<>(owner.getInsuranceSet());
    }

    @Override
    public Set<Property> getPropertiesForOwnerId(Long ownerId) {
        Owner owner = ownerDAO.findById(ownerId);
        return new HashSet<>(owner.getPropertySet());
    }
}
