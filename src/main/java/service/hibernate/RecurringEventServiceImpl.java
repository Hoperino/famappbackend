package service.hibernate;

import databasedao.interfaces.RecurringEventDAO;
import model.Document;
import model.Payment;
import model.RecurringEvent;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.DocumentService;
import service.interfaces.PaymentService;
import service.interfaces.RecurringEventService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Recurring event hibernate implementation
 */
@Service("recurringEventService")
@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class RecurringEventServiceImpl implements RecurringEventService {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private RecurringEventDAO recurringEventDAO;

    @Override
    public Long createRecurringEvent(RecurringEvent recurringEvent) {
        return recurringEventDAO.add(recurringEvent);
    }

    @Override
    public RecurringEvent findRecurringEventById(Long eventId) {
        return recurringEventDAO.findById(eventId);
    }

    @Override
    public void updateRecurringEvent(RecurringEvent recurringEvent) {
        recurringEventDAO.update(recurringEvent);
    }

    @Override
    public List<RecurringEvent> getAllRecurringEvent() {
        return new ArrayList<>(recurringEventDAO.getAll());
    }

    @Override
    public void removeRecurringEvent(Long eventId) {
        List<Payment> paymentList = new ArrayList<>(
                paymentService.getAllPaymentsForRecurringEvent(eventId));
        List<Document> documentList = new ArrayList<>(
                documentService.getAllDocumentsForRecurringEvent(eventId));
        for (Payment x: paymentList) {
            paymentService.removePayment(x.getId(), eventId, AssetType.RECURRING_EVENT);
        }
        for (Document x: documentList) {
            documentService.removeDocument(x.getId(), eventId, AssetType.RECURRING_EVENT);
        }
        recurringEventDAO.delete(recurringEventDAO.findById(eventId));
    }

    @Override
    public Set<Document> getDocumentsForRecurringEventId(Long eventId) {
        RecurringEvent recurringEvent = recurringEventDAO.findById(eventId);
        return new HashSet<>(recurringEvent.getDocumentSet());
    }

    @Override
    public Set<Payment> getPaymentsForRecurringEventId(Long eventId) {
        RecurringEvent recurringEvent = recurringEventDAO.findById(eventId);
        return new HashSet<>(recurringEvent.getPaymentSet());
    }
}
