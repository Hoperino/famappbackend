package service.hibernate;

import databasedao.interfaces.*;
import model.Document;
import model.Payment;
import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.DocumentService;
import service.interfaces.PaymentService;
import service.interfaces.PropertyService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Property service hibernate implementation
 */
@Service("propertyService")
@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class PropertyServiceImpl implements PropertyService {

    @Autowired
    private VehicleDAO vehicleDAO;

    @Autowired
    private RealEstateDAO realEstateDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private PropertyDAO propertyDAO;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private DocumentService documentService;

    @Override
    public Property findProperty(Long id) {
        return propertyDAO.findProperty(id);
    }

    @Override
    public Long createProperty(Stock propertyStock) {
        return stockDAO.add(propertyStock);
    }

    @Override
    public Long createProperty(RealEstate propertyRealEstate) {
        return realEstateDAO.add(propertyRealEstate);
    }

    @Override
    public Long createProperty(Vehicle propertyVehicle) {
        return vehicleDAO.add(propertyVehicle);
    }

    @Override
    public void updateProperty(Stock propertyStock) {
        stockDAO.update(propertyStock);
    }

    @Override
    public void updateProperty(RealEstate propertyRealEstate) {
        realEstateDAO.update(propertyRealEstate);
    }

    @Override
    public void updateProperty(Vehicle propertyVehicle) {
        vehicleDAO.update(propertyVehicle);
    }

    @Override
    public void removeProperty(Long propertyId) {
        Property property = propertyDAO.findProperty(propertyId);
        List<Payment> paymentList = new ArrayList<>(
                paymentService.getAllPaymentsForProperty(propertyId));
        List<Document> documentList = new ArrayList<>(
                documentService.getAllDocumentsForProperty(propertyId));

        if (property instanceof RealEstate) {
            for (Payment x: paymentList) {
                paymentService.removePayment(x.getId(), propertyId, AssetType.REAL_ESTATE);
            }
            for (Document x: documentList) {
                documentService.removeDocument(x.getId(), propertyId, AssetType.REAL_ESTATE);
            }
            propertyDAO.removeProperty(propertyId);
        } else if (property instanceof Stock) {
            for (Payment x: paymentList) {
                paymentService.removePayment(x.getId(), propertyId, AssetType.STOCK);
            }
            for (Document x: documentList) {
                documentService.removeDocument(x.getId(), propertyId, AssetType.STOCK);
            }
            propertyDAO.removeProperty(propertyId);
        } else {
            for (Payment x: paymentList) {
                paymentService.removePayment(x.getId(), propertyId, AssetType.VEHICLE);
            }
            for (Document x: documentList) {
                documentService.removeDocument(x.getId(), propertyId, AssetType.VEHICLE);
            }
            propertyDAO.removeProperty(propertyId);
        }

    }

    @Override
    public List<Stock> getAllStocks() {
        return new ArrayList<>(stockDAO.getAll());
    }

    @Override
    public List<RealEstate> getAllRealEstates() {
        return new ArrayList<>(realEstateDAO.getAll());
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        return new ArrayList<>(vehicleDAO.getAll());
    }

    @Override
    public List<Property> getAllProperties() {
        List<Property> listOfProperties = new ArrayList<>();
        listOfProperties.addAll(stockDAO.getAll());
        listOfProperties.addAll(realEstateDAO.getAll());
        listOfProperties.addAll(vehicleDAO.getAll());
        return listOfProperties;
    }

    @Override
    public Set<Document> getDocumentsForPropertyId(Long propertyId) {
        Property property = propertyDAO.findProperty(propertyId);
        return new HashSet<>(property.getDocumentSet());
    }

    @Override
    public Set<Payment> getPaymentsForPropertyId(Long propertyId) {
        Property property = propertyDAO.findProperty(propertyId);
        return new HashSet<>(property.getPaymentSet());
    }
}
