package service.hibernate;

import databasedao.interfaces.InsuranceDAO;
import databasedao.interfaces.PaymentDAO;
import databasedao.interfaces.PropertyDAO;
import databasedao.interfaces.RecurringEventDAO;
import model.Insurance;
import model.Payment;
import model.RecurringEvent;
import model.property.Property;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.PaymentService;

import java.util.*;

/**
 * Payment service hibernate implementation
 */
@Service("paymentService")
@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentDAO paymentDAO;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Autowired
    private RecurringEventDAO recurringEventDAO;

    @Autowired
    private PropertyDAO propertyDAO;

    @Override
    public Long createPayment(Payment payment, Long assetId, AssetType assetType) {
        Long newPaymentId = paymentDAO.add(payment);
        switch (assetType) {
            case REAL_ESTATE:
            case VEHICLE:
            case STOCK:
                this.addPaymentToProperty(assetId, newPaymentId);break;
            case RECURRING_EVENT:
                this.addPaymentToRecurringEvent(assetId, newPaymentId);break;
            case INSURANCE:
                this.addPaymentToInsurance(assetId, newPaymentId);break;
        }
        return newPaymentId;
    }

    @Override
    public Payment findPaymentById(Long paymentId) {
        return paymentDAO.findById(paymentId);
    }

    @Override
    public void updatePayment(Payment payment) {
        paymentDAO.update(payment);
    }

    @Override
    public List<Payment> getAllPayments() {
        return new ArrayList<>(paymentDAO.getAll());
    }

    @Override
    public void removePayment(Long paymentId, Long assetId, AssetType assetType) {
        switch (assetType) {
            case INSURANCE:
                this.removePaymentFromInsurance(assetId, paymentId);
                paymentDAO.delete(paymentDAO.findById(paymentId));
                break;
            case RECURRING_EVENT:
                this.removePaymentFromRecurringEvent(assetId, paymentId);
                paymentDAO.delete(paymentDAO.findById(paymentId));
                break;
            case STOCK:
            case VEHICLE:
            case REAL_ESTATE:
                this.removePaymentFromProperty(assetId, paymentId);
                paymentDAO.delete(paymentDAO.findById(paymentId));
                break;
            default: break;
        }
    }

    @Override
    public List<Payment> getAllPaymentsForProperty(Long propertyId) {
        Property property = propertyDAO.findProperty(propertyId);
        return new ArrayList<>(property.getPaymentSet());
    }

    @Override
    public List<Payment> getAllPaymentsForRecurringEvent(Long eventId) {
        RecurringEvent event = recurringEventDAO.findById(eventId);
        return new ArrayList<>(event.getPaymentSet());
    }

    @Override
    public List<Payment> getAllPaymentsForInsurance(Long insuranceId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        return new ArrayList<>(insurance.getPaymentSet());
    }

    @Override
    public void addPaymentToInsurance(Long insuranceId, Long paymentId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        Payment payment = paymentDAO.findById(paymentId);
        insurance.getPaymentSet().add(payment);
        insuranceDAO.update(insurance);
    }

    @Override
    public void addPaymentToRecurringEvent(Long eventId, Long paymentId) {
        RecurringEvent event = recurringEventDAO.findById(eventId);
        Payment payment = paymentDAO.findById(paymentId);
        event.getPaymentSet().add(payment);
        recurringEventDAO.update(event);
    }

    @Override
    public void addPaymentToProperty(Long propertyId, Long paymentId) {
        Property property = propertyDAO.findProperty(propertyId);
        Payment payment = paymentDAO.findById(paymentId);
        property.getPaymentSet().add(payment);
        propertyDAO.updateProperty(property);
    }

    @Override
    public void removePaymentFromProperty(Long propertyId, Long paymentId) {
        Property property = propertyDAO.findProperty(propertyId);
        // Payment payment = paymentDAO.findById(paymentId);

        /**
         * This is a workaround implementation for a bug!
         * In hibernate 5.*.* If you use set and override hashCode(),
         * methods Set.contains() and Set.remove() do not work
         * */
        Iterator<Payment> iterator = property.getPaymentSet().iterator();
        Set<Payment> paymentSet = new HashSet<>();
        while (iterator.hasNext()) {
            Payment val = iterator.next();
            if (!((long) val.getId() == paymentId)) {
                paymentSet.add(val);
            }
        }
        // property.getPaymentSet().remove(payment);
        property.setPaymentSet(paymentSet);
        propertyDAO.updateProperty(property);
    }

    @Override
    public void removePaymentFromRecurringEvent(Long eventId, Long paymentId) {
        RecurringEvent event = recurringEventDAO.findById(eventId);
        // Payment payment = paymentDAO.findById(paymentId);

        /**
         * This is a workaround implementation for a bug!
         * In hibernate 5.*.* If you use set and override hashCode(),
         * methods Set.contains() and Set.remove() do not work
         * */
        Iterator<Payment> iterator = event.getPaymentSet().iterator();
        Set<Payment> paymentSet = new HashSet<>();
        while (iterator.hasNext()) {
            Payment val = iterator.next();
            if (!((long) val.getId() == paymentId)) {
                paymentSet.add(val);
            }
        }
        // event.getPaymentSet().remove(payment);
        event.setPaymentSet(paymentSet);
        recurringEventDAO.update(event);
    }

    @Override
    public void removePaymentFromInsurance(Long insuranceId, Long paymentId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        // Payment payment = paymentDAO.findById(paymentId);

        /**
         * This is a workaround implementation for a bug!
         * In hibernate 5.*.* If you use set and override hashCode(),
         * methods Set.contains() and Set.remove() do not work
         * */
        Iterator<Payment> iterator = insurance.getPaymentSet().iterator();
        Set<Payment> paymentSet = new HashSet<>();
        while (iterator.hasNext()) {
            Payment val = iterator.next();
            if (!((long) val.getId() == paymentId)) {
                paymentSet.add(val);
            }
        }
        // insurance.getPaymentSet().remove(payment);
        insurance.setPaymentSet(paymentSet);
        insuranceDAO.update(insurance);
    }
}
