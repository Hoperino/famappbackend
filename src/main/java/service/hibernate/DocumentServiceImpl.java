package service.hibernate;

import databasedao.interfaces.DocumentDAO;
import databasedao.interfaces.InsuranceDAO;
import databasedao.interfaces.PropertyDAO;
import databasedao.interfaces.RecurringEventDAO;
import model.Document;
import model.Insurance;
import model.RecurringEvent;
import model.property.Property;
import model.utils.AssetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.DocumentService;

import java.util.*;

/**
 * Document service implementation
 */
@Service("documentService")
@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentDAO documentDAO;

    @Autowired
    private PropertyDAO propertyDAO;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Autowired
    private RecurringEventDAO recurringEventDAO;

    @Override
    public Long createDocument(Document document, Long assetId, AssetType assetType) {
        Long newDocumentId = documentDAO.add(document);
        switch (assetType) {
            case REAL_ESTATE:
            case VEHICLE:
            case STOCK:
                this.addDocumentToProperty(assetId, newDocumentId);break;
            case RECURRING_EVENT:
                this.addDocumentToRecurringEvent(assetId, newDocumentId);break;
            case INSURANCE:
                this.addDocumentToInsurance(assetId, newDocumentId);break;
        }
        return newDocumentId;
    }

    @Override
    public Document findDocumentById(Long documentId) {
        return documentDAO.findById(documentId);
    }

    @Override
    public void updateDocument(Document document) {
        documentDAO.update(document);
    }

    @Override
    public List<Document> getAllDocuments() {
        return new ArrayList<>(documentDAO.getAll());
    }

    @Override
    public void removeDocument(Long documentId, Long assetId, AssetType assetType) {
        switch (assetType) {
            case INSURANCE:
                this.removeDocumentFromInsurance(assetId, documentId);
                documentDAO.delete(documentDAO.findById(documentId));
                break;
            case RECURRING_EVENT:
                this.removeDocumentFromRecurringEvent(assetId, documentId);
                documentDAO.delete(documentDAO.findById(documentId));
                break;
            case STOCK:
            case VEHICLE:
            case REAL_ESTATE:
                this.removeDocumentFromProperty(assetId, documentId);
                documentDAO.delete(documentDAO.findById(documentId));
                break;
            default: break;
        }
    }

    @Override
    public List<Document> getAllDocumentsForProperty(Long propertyId) {
        Property property = propertyDAO.findProperty(propertyId);
        return new ArrayList<>(property.getDocumentSet());
    }

    @Override
    public List<Document> getAllDocumentsForInsurance(Long insuranceId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        return new ArrayList<>(insurance.getDocumentSet());
    }

    @Override
    public List<Document> getAllDocumentsForRecurringEvent(Long eventId) {
        RecurringEvent event = recurringEventDAO.findById(eventId);
        return new ArrayList<>(event.getDocumentSet());
    }

    @Override
    public void addDocumentToInsurance(Long insuranceId, Long documentId) {
        Document document = documentDAO.findById(documentId);
        Insurance insurance = insuranceDAO.findById(insuranceId);
        insurance.getDocumentSet().add(document);
        insuranceDAO.update(insurance);
    }

    @Override
    public void addDocumentToRecurringEvent(Long eventId, Long documentId) {
        RecurringEvent event = recurringEventDAO.findById(eventId);
        Document document = documentDAO.findById(documentId);
        event.getDocumentSet().add(document);
        recurringEventDAO.update(event);
    }

    @Override
    public void addDocumentToProperty(Long propertyId, Long documentId) {
        Document document = documentDAO.findById(documentId);
        Property property = propertyDAO.findProperty(propertyId);
        property.getDocumentSet().add(document);
        propertyDAO.updateProperty(property);
    }

    @Override
    public void removeDocumentFromProperty(Long propertyId, Long documentId) {
        Property property = propertyDAO.findProperty(propertyId);
        // Document document = documentDAO.findById(documentId);

        /**
         * This is a workaround implementation for a bug!
         * In hibernate 5.*.* If you use set and override hashCode(),
         * methods Set.contains() and Set.remove() do not work
         * */
        Iterator<Document> iterator = property.getDocumentSet().iterator();
        Set<Document> documentSet = new HashSet<>();
        while (iterator.hasNext()) {
            Document val = iterator.next();
            if (!((long) val.getId() == documentId)) {
                documentSet.add(val);
            }
        }
        //property.getDocumentSet().remove(document);
        property.setDocumentSet(documentSet);
        propertyDAO.updateProperty(property);
    }

    @Override
    public void removeDocumentFromRecurringEvent(Long eventId, Long documentId) {
        RecurringEvent event = recurringEventDAO.findById(eventId);
        // Document document = documentDAO.findById(documentId);

        /**
         * This is a workaround implementation for a bug!
         * In hibernate 5.*.* If you use set and override hashCode(),
         * methods Set.contains() and Set.remove() do not work
         * */
        Iterator<Document> iterator = event.getDocumentSet().iterator();
        Set<Document> documentSet = new HashSet<>();
        while (iterator.hasNext()) {
            Document val = iterator.next();
            if (!((long) val.getId() == documentId)) {
                documentSet.add(val);
            }
        }
        // event.getDocumentSet().remove(document);
        event.setDocumentSet(documentSet);
        recurringEventDAO.update(event);
    }

    @Override
    public void removeDocumentFromInsurance(Long insuranceId, Long documentId) {
        Insurance insurance = insuranceDAO.findById(insuranceId);
        // Document document = documentDAO.findById(documentId);

        /**
         * This is a workaround implementation for a bug!
         * In hibernate 5.*.* If you use set and override hashCode(),
         * methods Set.contains() and Set.remove() do not work
         * */
        Iterator<Document> iterator = insurance.getDocumentSet().iterator();
        Set<Document> documentSet = new HashSet<>();
        while (iterator.hasNext()) {
            Document val = iterator.next();
            if (!((long) val.getId() == documentId)) {
                documentSet.add(val);
            }
        }
        insurance.setDocumentSet(documentSet);
        // insurance.getDocumentSet().remove(document);
        insuranceDAO.update(insurance);
    }
}
