package service.interfaces;

import model.Document;
import model.Insurance;
import model.Payment;

import java.util.List;
import java.util.Set;

/**
 * Insurance service
 */
public interface InsuranceService {

    /** Add insurance */
    Long createInsurance(Insurance insurance);

    /** Find insurance */
    Insurance findInsuranceById(Long insuranceId);

    /** Update insurance */
    void updateInsurance(Insurance insurance);

    /** Get all insurances */
    List<Insurance> getAllInsurances();

    /** Delete insurance */
    void removeInsurance(Long insuranceId);

    /** Refresh documents */
    Set<Document> getDocumentsForInsuranceId(Long insuranceId);

    /** Refresh payments */
    Set<Payment> getPaymentsForInsuranceId(Long insuranceId);
}
