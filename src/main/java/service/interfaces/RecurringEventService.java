package service.interfaces;

import model.Document;
import model.Payment;
import model.RecurringEvent;

import java.util.List;
import java.util.Set;

/**
 * Recurring event service
 */
public interface RecurringEventService {

    /** Add recurring event */
    Long createRecurringEvent(RecurringEvent recurringEvent);

    /** Find recurring event */
    RecurringEvent findRecurringEventById(Long eventId);

    /** Update recurring event */
    void updateRecurringEvent(RecurringEvent recurringEvent);

    /** Get all recurring events */
    List<RecurringEvent> getAllRecurringEvent();

    /** Remove Recurring event and its payments and documents */
    void removeRecurringEvent(Long eventId);

    /** Refresh documents */
    Set<Document> getDocumentsForRecurringEventId(Long eventId);

    /** Refresh payments */
    Set<Payment> getPaymentsForRecurringEventId(Long eventId);
}
