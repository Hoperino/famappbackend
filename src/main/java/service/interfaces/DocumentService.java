package service.interfaces;

import model.Document;
import model.utils.AssetType;

import java.util.List;

/**
 * Document service
 */
public interface DocumentService {

    /** Add document */
    Long createDocument(Document document, Long assetId, AssetType assetType);

    /** Find document */
    Document findDocumentById(Long documentId);

    /** Update document */
    void updateDocument(Document document);

    /** Get all documents */
    List<Document> getAllDocuments();

    /** Remove document and its associations */
    void removeDocument(Long documentId, Long assetId, AssetType assetType);

    /** Get all documents for a Property */
    List<Document> getAllDocumentsForProperty(Long propertyId);

    /** Get all documents for a Insurance */
    List<Document> getAllDocumentsForInsurance(Long insuranceId);

    /** Get all documents for a Recurring Event */
    List<Document> getAllDocumentsForRecurringEvent(Long eventId);

    /** Add document for Insurance */
    void addDocumentToInsurance(Long insuranceId, Long documentId);

    /** Add document for Recurring Event */
    void addDocumentToRecurringEvent(Long eventId, Long documentId);

    /** Add document for Property*/
    void addDocumentToProperty(Long propertyId, Long documentId);

    /** Remove association between Payment and Property */
    void removeDocumentFromProperty(Long propertyId, Long documentId);

    /** Remove association between Payment and Recurring Event */
    void removeDocumentFromRecurringEvent(Long eventId, Long documentId);

    /** Remove association between Payment and Insurance */
    void removeDocumentFromInsurance(Long insuranceId, Long documentId);
}
