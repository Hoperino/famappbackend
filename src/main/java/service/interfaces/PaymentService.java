package service.interfaces;

import model.Payment;
import model.utils.AssetType;

import java.util.List;

/**
 * Payment service
 */
public interface PaymentService {

    /** Add payment */
    Long createPayment(Payment payment, Long assetId, AssetType assetType);

    /** Find payment */
    Payment findPaymentById(Long paymentId);

    /** Update payment */
    void updatePayment(Payment payment);

    /** Get all payments */
    List<Payment> getAllPayments();

    /** Remove payment and its associations */
    void removePayment(Long paymentId, Long assetId, AssetType assetType);

    /** Get all payments for a property */
    List<Payment> getAllPaymentsForProperty(Long propertyId);

    /** Get all payments for a recurringEvent */
    List<Payment> getAllPaymentsForRecurringEvent(Long eventId);

    /** Get all payments for a insurance */
    List<Payment> getAllPaymentsForInsurance(Long insuranceId);

    /** Add payment for insurance */
    void addPaymentToInsurance(Long insuranceId, Long paymentId);

    /** Add payment for insurance */
    void addPaymentToRecurringEvent(Long eventId, Long paymentId);

    /** Add payment for Property */
    void addPaymentToProperty(Long propertyId, Long paymentId);

    /** Remove association between payment and Property */
    void removePaymentFromProperty(Long propertyId, Long paymentId);

    /** Remove association between payment and Recurring Event */
    void removePaymentFromRecurringEvent(Long eventId, Long paymentId);

    /** Remove association between payment and insurance */
    void removePaymentFromInsurance(Long insuranceId, Long paymentId);
}
