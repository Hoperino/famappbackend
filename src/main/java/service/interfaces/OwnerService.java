package service.interfaces;


import model.Insurance;
import model.Owner;
import model.property.Property;

import java.util.List;
import java.util.Set;

/**
 * Owner service
 */
public interface OwnerService {

    /** Add owner */
    Long createOwner(Owner owner);

    /** Update owner */
    void updateOwner(Owner owner);

    /** Find owner */
    Owner findOwnerById(Long ownerId);

    /** Get all family members */
    List<Owner> getAllOwners();

    /** Remove owner and his properties, insurances and respective documents and payments */
    void removeOwner(Long ownerId);

    /** Refresh insurances */
    Set<Insurance> getInsurancesForOwnerId(Long ownerId);

    /** Refresh properties */
    Set<Property> getPropertiesForOwnerId(Long ownerId);
}
