package service.interfaces;

import model.Document;
import model.Payment;
import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;

import java.util.List;
import java.util.Set;

/**
 * Property service object that provides actions for all types of property
 */
public interface PropertyService {

    /** Find Property */
    Property findProperty(Long id);

    /**
     * Overload the Property creation
     * Should be called when owner for the Property is set
     * */
    Long createProperty(Stock propertyStock);

    /* Overloaded */
    Long createProperty(RealEstate propertyRealEstate);

    /* Overloaded */
    Long createProperty(Vehicle propertyVehicle);

    /** Overload the Property update*/
    void updateProperty(Stock propertyStock);

    /* Overloaded */
    void updateProperty(RealEstate propertyRealEstate);

    /* Overloaded */
    void updateProperty(Vehicle propertyVehicle);

    /** Property delete*/
    void removeProperty(Long propertyId);

    /** Get all Stocks */
    List<Stock> getAllStocks();

    /** Get all Real Estates */
    List<RealEstate> getAllRealEstates();

    /** Get all Vehicle */
    List<Vehicle> getAllVehicles();

    /** Get all properties*/
    List<Property> getAllProperties();

    /** Refresh documents */
    Set<Document> getDocumentsForPropertyId(Long propertyId);

    /** Refresh payments */
    Set<Payment> getPaymentsForPropertyId(Long propertyId);

}
