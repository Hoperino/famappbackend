package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import model.Insurance;
import model.Owner;
import model.RecurringEvent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.OwnerService;
import service.interfaces.RecurringEventService;

import java.util.List;

/**
 * RecurringEventService test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class RecurringEventServiceTest {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private RecurringEventService recurringEventService;

    @Test
    @Rollback
    public void testMethodsInsuranceService() {
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        RecurringEvent event = new RecurringEvent();
        event.setTitle("Dummy");

        Assert.assertTrue(TestTransaction.isActive());

        ownerService.createOwner(owner);

        // create
        recurringEventService.createRecurringEvent(event);
        List<RecurringEvent> recurringEventList = recurringEventService.getAllRecurringEvent();

        Assert.assertTrue(recurringEventList.size() == 1);
        Assert.assertTrue(event.equals(recurringEventList.get(0)));

        // update
        event.setTitle("Dummy Changed");
        recurringEventService.updateRecurringEvent(event);

        recurringEventList = recurringEventService.getAllRecurringEvent();

        Assert.assertTrue(recurringEventList.get(0).getTitle().equals(event.getTitle()));

        // find by id
        recurringEventList = recurringEventService.getAllRecurringEvent();
        RecurringEvent event1 = recurringEventService.findRecurringEventById(recurringEventList.get(0).getId());

        Assert.assertTrue(event1.equals(event));

        // remove
        recurringEventService.removeRecurringEvent(event.getId());
        recurringEventList = recurringEventService.getAllRecurringEvent();

        Assert.assertTrue(recurringEventList.isEmpty());
    }
}
