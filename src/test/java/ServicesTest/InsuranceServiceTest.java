package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import model.Insurance;
import model.Owner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.InsuranceService;
import service.interfaces.OwnerService;

import java.util.List;

/**
 * InsuranceService test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class InsuranceServiceTest {

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private OwnerService ownerService;


    @Test
    @Rollback
    public void testMethodsInsuranceService() {
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);

        Assert.assertTrue(TestTransaction.isActive());

        ownerService.createOwner(owner);

        // create
        insuranceService.createInsurance(insurance);
        List<Insurance> insuranceList = insuranceService.getAllInsurances();

        Assert.assertTrue(insuranceList.size() == 1);
        Assert.assertTrue(insurance.equals(insuranceList.get(0)));

        // update
        insurance.setName("Dummy Changed");
        insuranceService.updateInsurance(insurance);

        insuranceList = insuranceService.getAllInsurances();

        Assert.assertTrue(insuranceList.get(0).getName().equals(insurance.getName()));

        // find by id
        insuranceList = insuranceService.getAllInsurances();
        Insurance insurance1 = insuranceService.findInsuranceById(insuranceList.get(0).getId());

        Assert.assertTrue(insurance1.equals(insurance));

        // remove
        insuranceService.removeInsurance(insurance1.getId());
        insuranceList = insuranceService.getAllInsurances();

        Assert.assertTrue(insuranceList.isEmpty());
    }
}
