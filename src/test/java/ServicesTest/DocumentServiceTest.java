package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import model.Document;
import model.Insurance;
import model.Owner;
import model.RecurringEvent;
import model.property.Vehicle;
import model.utils.AssetType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.*;

import java.util.List;

/**
 * DocumentService test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class DocumentServiceTest {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private RecurringEventService recurringEventService;

    @Autowired
    private DocumentService documentService;

    @Test
    @Rollback
    public void testMethodsDocumentService() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Assert.assertTrue(TestTransaction.isActive());

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);

        RecurringEvent recurringEvent = new RecurringEvent();
        recurringEvent.setTitle("Dummy");

        Document document1 = new Document();
        document1.setName("Dummy 1");

        Document document2 = new Document();
        document2.setName("Dummy 2");

        Document document3 = new Document();
        document3.setName("Dummy 3");

        ownerService.createOwner(owner);
        Long insuranceId = insuranceService.createInsurance(insurance);
        Long propertyId = propertyService.createProperty(vehicle);
        Long recurringEventId = recurringEventService.createRecurringEvent(recurringEvent);

        // create
        Long document1Id = documentService.createDocument(document1, propertyId, AssetType.VEHICLE);
        Long document2Id = documentService.createDocument(document2, insuranceId, AssetType.INSURANCE);
        Long document3Id = documentService.createDocument(document3, recurringEventId, AssetType.RECURRING_EVENT);

        List<Document> documentList = documentService.getAllDocuments();
        List<Vehicle> vehicleList = propertyService.getAllVehicles();
        List<Insurance> insuranceList = insuranceService.getAllInsurances();
        List<RecurringEvent> recurringEventList = recurringEventService.getAllRecurringEvent();

        Assert.assertTrue(documentList.size() == 3);
        Assert.assertTrue(vehicleList.get(0).getDocumentSet().size() == 1);
        Assert.assertTrue(vehicleList.get(0).getDocumentSet().contains(document1));
        Assert.assertTrue(insuranceList.get(0).getDocumentSet().size() == 1);
        Assert.assertTrue(insuranceList.get(0).getDocumentSet().contains(document2));
        Assert.assertTrue(recurringEventList.get(0).getDocumentSet().size() == 1);
        Assert.assertTrue(recurringEventList.get(0).getDocumentSet().contains(document3));

        // find by id
        Document documentClone1 = documentService.findDocumentById(document1Id);
        Document documentClone2 = documentService.findDocumentById(document2Id);
        Document documentClone3 = documentService.findDocumentById(document3Id);

        Assert.assertNotNull(documentClone1);
        Assert.assertTrue(document1.getName().equals(documentClone1.getName()));
        Assert.assertNotNull(documentClone2);
        Assert.assertTrue(document2.getName().equals(documentClone2.getName()));
        Assert.assertNotNull(documentClone3);
        Assert.assertTrue(document3.getName().equals(documentClone3.getName()));

        // update
        document1.setName("Changed Dummy 1");
        document2.setName("Changed Dummy 2");
        document3.setName("Changed Dummy 3");

        documentService.updateDocument(document1);
        documentService.updateDocument(document2);
        documentService.updateDocument(document3);

        documentClone1 = documentService.findDocumentById(document1Id);
        documentClone2 = documentService.findDocumentById(document2Id);
        documentClone3 = documentService.findDocumentById(document3Id);
        documentList = documentService.getAllDocuments();

        Assert.assertTrue(documentList.size() == 3);
        Assert.assertNotNull(documentClone1);
        Assert.assertTrue(document1.getName().equals(documentClone1.getName()));
        Assert.assertNotNull(documentClone2);
        Assert.assertTrue(document2.getName().equals(documentClone2.getName()));
        Assert.assertNotNull(documentClone3);
        Assert.assertTrue(document3.getName().equals(documentClone3.getName()));

        // get all payments for property
        List<Document> documentListForProperty = documentService.getAllDocumentsForProperty(propertyId);

        Assert.assertTrue(documentListForProperty.size() == 1);
        Assert.assertTrue(((long) documentListForProperty.get(0).getId()) == documentClone1.getId());

        //get all payments for insurance
        List<Document> documentListForInsurance = documentService.getAllDocumentsForInsurance(insuranceId);

        Assert.assertTrue(documentListForInsurance.size() == 1);
        Assert.assertTrue(((long) documentListForInsurance.get(0).getId()) == documentClone2.getId());

        // get all payments for recurring event
        List<Document> documentListForEvent = documentService.getAllDocumentsForRecurringEvent(recurringEventId);

        Assert.assertTrue(documentListForEvent.size() == 1);
        Assert.assertTrue(((long) documentListForEvent.get(0).getId()) == documentClone3.getId());

        // delete
        documentService.removeDocument(document1Id, propertyId, AssetType.VEHICLE);
        documentListForProperty = documentService.getAllDocumentsForProperty(propertyId);
        documentList = documentService.getAllDocuments();

        Assert.assertTrue(documentList.size() == 2);
        Assert.assertTrue(documentListForProperty.isEmpty());

        documentService.removeDocument(document2Id, insuranceId, AssetType.INSURANCE);
        documentListForInsurance = documentService.getAllDocumentsForInsurance(insuranceId);
        documentList = documentService.getAllDocuments();

        Assert.assertTrue(documentList.size() == 1);
        Assert.assertTrue(documentListForInsurance.isEmpty());

        documentService.removeDocument(document3Id, recurringEventId, AssetType.RECURRING_EVENT);
        documentListForEvent = documentService.getAllDocumentsForRecurringEvent(recurringEventId);
        documentList = documentService.getAllDocuments();

        Assert.assertTrue(documentList.isEmpty());
        Assert.assertTrue(documentListForEvent.isEmpty());
    }
}
