package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import model.Insurance;
import model.Owner;
import model.Payment;
import model.RecurringEvent;
import model.property.Property;
import model.property.Vehicle;
import model.utils.AssetType;
import model.utils.PaymentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.*;

import java.util.List;

/**
 * PaymentService test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class PaymentServiceTest {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private RecurringEventService recurringEventService;

    @Autowired
    private PaymentService paymentService;

    @Test
    @Rollback
    public void testMethodsPaymentService() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Assert.assertTrue(TestTransaction.isActive());

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);

        RecurringEvent recurringEvent = new RecurringEvent();
        recurringEvent.setTitle("Dummy");

        Payment payment1 = new Payment();
        payment1.setName("Dummy Pay 1");
        payment1.setType(PaymentType.OTHER);
        payment1.setValue(1.0);

        Payment payment2 = new Payment();
        payment2.setName("Dummy Pay 2");
        payment2.setType(PaymentType.OTHER);
        payment2.setValue(1.0);

        Payment payment3 = new Payment();
        payment3.setName("Dummy Pay 3");
        payment3.setType(PaymentType.OTHER);
        payment3.setValue(1.0);

        ownerService.createOwner(owner);
        Long insuranceId = insuranceService.createInsurance(insurance);
        Long propertyId = propertyService.createProperty(vehicle);
        Long recurringEventId = recurringEventService.createRecurringEvent(recurringEvent);

        // create
        Long payment1Id = paymentService.createPayment(payment1, propertyId, AssetType.VEHICLE);
        Long payment2Id = paymentService.createPayment(payment2, insuranceId, AssetType.INSURANCE);
        Long payment3Id = paymentService.createPayment(payment3, recurringEventId, AssetType.RECURRING_EVENT);

        List<Payment> paymentList = paymentService.getAllPayments();
        List<Vehicle> vehicleList = propertyService.getAllVehicles();
        List<Insurance> insuranceList = insuranceService.getAllInsurances();
        List<RecurringEvent> recurringEventList = recurringEventService.getAllRecurringEvent();

        Assert.assertTrue(paymentList.size() == 3);
        Assert.assertTrue(vehicleList.get(0).getPaymentSet().size() == 1);
        Assert.assertTrue(vehicleList.get(0).getPaymentSet().contains(payment1));
        Assert.assertTrue(insuranceList.get(0).getPaymentSet().size() == 1);
        Assert.assertTrue(insuranceList.get(0).getPaymentSet().contains(payment2));
        Assert.assertTrue(recurringEventList.get(0).getPaymentSet().size() == 1);
        Assert.assertTrue(recurringEventList.get(0).getPaymentSet().contains(payment3));

        // find by id
        Payment paymentClone1 = paymentService.findPaymentById(payment1Id);
        Payment paymentClone2 = paymentService.findPaymentById(payment2Id);
        Payment paymentClone3 = paymentService.findPaymentById(payment3Id);

        Assert.assertNotNull(paymentClone1);
        Assert.assertTrue(payment1.getName().equals(paymentClone1.getName()));
        Assert.assertNotNull(paymentClone2);
        Assert.assertTrue(payment2.getName().equals(paymentClone2.getName()));
        Assert.assertNotNull(paymentClone3);
        Assert.assertTrue(payment3.getName().equals(paymentClone3.getName()));

        // update
        payment1.setName("Changed Dummy 1");
        payment2.setName("Changed Dummy 2");
        payment3.setName("Changed Dummy 3");

        paymentService.updatePayment(payment1);
        paymentService.updatePayment(payment2);
        paymentService.updatePayment(payment3);

        paymentClone1 = paymentService.findPaymentById(payment1Id);
        paymentClone2 = paymentService.findPaymentById(payment2Id);
        paymentClone3 = paymentService.findPaymentById(payment3Id);
        paymentList = paymentService.getAllPayments();

        Assert.assertTrue(paymentList.size() == 3);
        Assert.assertNotNull(paymentClone1);
        Assert.assertTrue(payment1.getName().equals(paymentClone1.getName()));
        Assert.assertNotNull(paymentClone2);
        Assert.assertTrue(payment2.getName().equals(paymentClone2.getName()));
        Assert.assertNotNull(paymentClone3);
        Assert.assertTrue(payment3.getName().equals(paymentClone3.getName()));

        // get all payments for property
        List<Payment> paymentListForProperty = paymentService.getAllPaymentsForProperty(propertyId);

        Assert.assertTrue(paymentListForProperty.size() == 1);
        Assert.assertTrue(((long) paymentListForProperty.get(0).getId()) == paymentClone1.getId());

        //get all payments for insurance
        List<Payment> paymentListForInsurance = paymentService.getAllPaymentsForInsurance(insuranceId);

        Assert.assertTrue(paymentListForInsurance.size() == 1);
        Assert.assertTrue(((long) paymentListForInsurance.get(0).getId()) == paymentClone2.getId());

        // get all payments for recurring event
        List<Payment> paymentListForEvent = paymentService.getAllPaymentsForRecurringEvent(recurringEventId);

        Assert.assertTrue(paymentListForEvent.size() == 1);
        Assert.assertTrue(((long) paymentListForEvent.get(0).getId()) == paymentClone3.getId());

        // delete
        paymentService.removePayment(payment1Id, propertyId, AssetType.VEHICLE);
        paymentListForProperty = paymentService.getAllPaymentsForProperty(propertyId);
        paymentList = paymentService.getAllPayments();

        Assert.assertTrue(paymentList.size() == 2);
        Assert.assertTrue(paymentListForProperty.isEmpty());

        paymentService.removePayment(payment2Id, insuranceId, AssetType.INSURANCE);
        paymentListForInsurance = paymentService.getAllPaymentsForInsurance(insuranceId);
        paymentList = paymentService.getAllPayments();

        Assert.assertTrue(paymentList.size() == 1);
        Assert.assertTrue(paymentListForInsurance.isEmpty());

        paymentService.removePayment(payment3Id, recurringEventId, AssetType.RECURRING_EVENT);
        paymentListForEvent = paymentService.getAllPaymentsForRecurringEvent(recurringEventId);
        paymentList = paymentService.getAllPayments();

        Assert.assertTrue(paymentList.isEmpty());
        Assert.assertTrue(paymentListForEvent.isEmpty());
    }
}
