package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import model.Owner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.OwnerService;

import java.util.List;

/**
 * Testing OwnerService implementation
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class OwnerServiceTest {

    @Autowired
    private OwnerService ownerService;

    @Test
    @Rollback
    public void testMethodsOwnerService() {
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Assert.assertTrue(TestTransaction.isActive());

        // create
        ownerService.createOwner(owner);
        List<Owner> ownerList = ownerService.getAllOwners();

        Assert.assertTrue(ownerList.size() == 1);
        Assert.assertTrue(owner.equals(ownerList.get(0)));

        // update
        owner.setFirstName("Dummy Changed");
        ownerService.updateOwner(owner);

        ownerList = ownerService.getAllOwners();

        Assert.assertTrue(ownerList.get(0).getFirstName().equals(owner.getFirstName()));

        // find by id
        ownerList = ownerService.getAllOwners();
        Owner owner1 = ownerService.findOwnerById(ownerList.get(0).getId());

        Assert.assertTrue(owner1.equals(owner));

        // remove
        ownerService.removeOwner(owner1.getId());
        ownerList = ownerService.getAllOwners();

        Assert.assertTrue(ownerList.isEmpty());
    }
}
