package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import databasedao.interfaces.InsuranceDAO;
import databasedao.interfaces.OwnerDAO;
import databasedao.interfaces.PaymentDAO;
import databasedao.interfaces.VehicleDAO;
import model.Avatar;
import model.Insurance;
import model.Owner;
import model.Payment;
import model.property.Vehicle;
import model.utils.AssetType;
import model.utils.PaymentType;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.InsuranceService;
import service.interfaces.OwnerService;
import service.interfaces.PaymentService;
import service.interfaces.PropertyService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Testing the cascading deletion of Owner -> Insurance -> Payment
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class DeleteOwnerAndCascadeDownDeletionTest {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private PaymentService paymentService;

    @Test
    @Rollback
    public void cascadeDeleteTest() {
        //init
        Avatar avatar = new Avatar();
        avatar.setImage("image".getBytes());

        Owner owner1 = new Owner();
        owner1.setFirstName("Dummy");
        owner1.setLastName("Dummy");
        owner1.setAvatar(avatar);

        Owner owner2 = new Owner();
        owner2.setFirstName("Dummy Dumm");
        owner2.setLastName("Dummy Dumm");

        Insurance insurance1 = new Insurance();
        insurance1.setName("Dummy");
        insurance1.setUid("Dummy");

        Insurance insurance2 = new Insurance();
        insurance2.setName("Dummy Dumm");
        insurance2.setUid("Dummy Dumm");

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy Dum");

        // is assigned to insurance 1
        Payment payment1 = new Payment();
        payment1.setName("Dummy Pay 1");
        payment1.setType(PaymentType.OTHER);
        payment1.setValue(1.0);

        // is assigned to vehicle
        Payment payment2 = new Payment();
        payment2.setName("Dummy Pay 2");
        payment2.setType(PaymentType.OTHER);
        payment2.setValue(1.0);

        // is assigned to insurance 2
        Payment payment3 = new Payment();
        payment3.setName("Dummy Pay 3");
        payment3.setType(PaymentType.OTHER);
        payment3.setValue(1.0);

        //hibernate dao magic
        ownerService.createOwner(owner1);
        ownerService.createOwner(owner2);

        insurance1.setOwner(owner1);
        insurance2.setOwner(owner2);
        vehicle.setOwner(owner1);

        Long insurance1Id = insuranceService.createInsurance(insurance1);
        Long insurance2Id = insuranceService.createInsurance(insurance2);
        Long vehicleId = propertyService.createProperty(vehicle);

        paymentService.createPayment(payment1, insurance1Id, AssetType.INSURANCE);
        paymentService.createPayment(payment2, vehicleId, AssetType.VEHICLE);
        paymentService.createPayment(payment3, insurance2Id, AssetType.INSURANCE);

        List<Insurance> insuranceList = insuranceService.getAllInsurances();
        List<Owner> ownerList = ownerService.getAllOwners();
        List<Payment> paymentList = paymentService.getAllPayments();
        List<Vehicle> vehicleList = propertyService.getAllVehicles();

        Assert.assertTrue(insuranceList.size() == 2);
        Assert.assertTrue(ownerList.size() == 2);
        Assert.assertTrue(paymentList.size() == 3);
        Assert.assertTrue(vehicleList.size() == 1);

        Assert.assertTrue(insuranceList.get(0).getOwner().equals(owner1));
        Assert.assertTrue(insuranceList.get(1).getOwner().equals(owner2));
        Assert.assertTrue(vehicleList.get(0).getOwner().equals(owner1));
        Assert.assertTrue(insuranceList.get(0).getPaymentSet().size() == 1);

        ownerService.removeOwner(owner1.getId());
        insuranceList = insuranceService.getAllInsurances();
        ownerList = ownerService.getAllOwners();
        paymentList = paymentService.getAllPayments();
        vehicleList = propertyService.getAllVehicles();

        Assert.assertTrue(ownerList.size() == 1);
        Assert.assertTrue(insuranceList.size() == 1);
        Assert.assertTrue(paymentList.size() == 1);
        Assert.assertTrue(vehicleList.isEmpty());

        Assert.assertTrue(insuranceList.get(0).getOwner().equals(owner2));
    }
}
