package ServicesTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import model.Owner;
import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.OwnerService;
import service.interfaces.PropertyService;

import java.util.List;

/**
 * PropertyService test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class})
@Transactional
public class PropertyServiceTest {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private OwnerService ownerService;

    @Test
    @Rollback
    public void testMethodsPropertyService() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Assert.assertTrue(TestTransaction.isActive());

        ownerService.createOwner(owner);

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);

        RealEstate realEstate = new RealEstate();
        realEstate.setName("Dummy");
        realEstate.setOwner(owner);

        Stock stock = new Stock();
        stock.setName("Dummy");
        stock.setOwner(owner);

        // create
        propertyService.createProperty(vehicle);
        propertyService.createProperty(realEstate);
        propertyService.createProperty(stock);

        List<Vehicle> vehicleList = propertyService.getAllVehicles();
        List<RealEstate> realEstateList = propertyService.getAllRealEstates();
        List<Stock> stockList = propertyService.getAllStocks();
        List<Property> propertyList = propertyService.getAllProperties();

        Assert.assertTrue(vehicleList.size() == 1);
        Assert.assertTrue(realEstateList.size() == 1);
        Assert.assertTrue(stockList.size() == 1);
        Assert.assertTrue(propertyList.size() == 3);

        // update
        vehicle.setName("Dummy changed");
        realEstate.setName("Dummy changed");
        stock.setName("Dummy changed");

        propertyService.updateProperty(vehicle);
        propertyService.updateProperty(realEstate);
        propertyService.updateProperty(stock);

        vehicleList = propertyService.getAllVehicles();
        realEstateList = propertyService.getAllRealEstates();
        stockList = propertyService.getAllStocks();

        Assert.assertTrue(vehicle.getName().equals(vehicleList.get(0).getName()));
        Assert.assertTrue(realEstate.getName().equals(realEstateList.get(0).getName()));
        Assert.assertTrue(stock.getName().equals(stockList.get(0).getName()));

        // find by Id
        Property vehicleProperty = propertyService.findProperty(vehicleList.get(0).getId());
        Property realEstateProperty = propertyService.findProperty(realEstateList.get(0).getId());
        Property stockProperty = propertyService.findProperty(stockList.get(0).getId());

        Assert.assertNotNull(vehicleProperty);
        Assert.assertNotNull(realEstateProperty);
        Assert.assertNotNull(stockProperty);

        Assert.assertTrue(vehicleProperty instanceof Vehicle);
        Assert.assertTrue(realEstateProperty instanceof RealEstate);
        Assert.assertTrue(stockProperty instanceof Stock);

        // delete
        propertyService.removeProperty(vehicleProperty.getId());
        vehicleProperty = propertyService.findProperty(vehicleList.get(0).getId());
        vehicleList = propertyService.getAllVehicles();
        propertyList = propertyService.getAllProperties();

        Assert.assertNull(vehicleProperty);
        Assert.assertTrue(vehicleList.isEmpty());
        Assert.assertTrue(propertyList.size() == 2);

        propertyService.removeProperty(realEstateProperty.getId());
        realEstateProperty = propertyService.findProperty(realEstateList.get(0).getId());
        realEstateList = propertyService.getAllRealEstates();
        propertyList = propertyService.getAllProperties();

        Assert.assertNull(realEstateProperty);
        Assert.assertTrue(realEstateList.isEmpty());
        Assert.assertTrue(propertyList.size() == 1);

        propertyService.removeProperty(stockProperty.getId());
        stockProperty = propertyService.findProperty(stockList.get(0).getId());
        stockList = propertyService.getAllStocks();
        propertyList = propertyService.getAllProperties();

        Assert.assertNull(stockProperty);
        Assert.assertTrue(stockList.isEmpty());
        Assert.assertTrue(propertyList.isEmpty());
    }
}
