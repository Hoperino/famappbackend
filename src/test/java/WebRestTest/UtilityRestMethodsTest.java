package WebRestTest;

import config.DaoConfig;
import config.HibernateConfig;
import config.ServiceConfig;
import config.WebRestConfig;
import model.*;
import model.property.Property;
import model.property.Vehicle;
import model.utils.AssetType;
import model.utils.PaymentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import service.interfaces.*;
import web.rest.exceptions.IdNotFoundException;
import web.rest.exceptions.QueryParamsInvalidException;
import web.rest.utility.UtilityRestMethods;

/**
 * Testing the functionality of UtilityRestMethods
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class, ServiceConfig.class, WebRestConfig.class})
@Transactional
public class UtilityRestMethodsTest {

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private RecurringEventService recurringEventService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private UtilityRestMethods utilityRestMethods;

    @Test
    @Rollback
    public void testQueryParamValidation() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);

        RecurringEvent recurringEvent = new RecurringEvent();
        recurringEvent.setTitle("Dummy");

        ownerService.createOwner(owner);
        Long insuranceId = insuranceService.createInsurance(insurance);
        Long propertyId = propertyService.createProperty(vehicle);
        Long eventId = recurringEventService.createRecurringEvent(recurringEvent);

        try {
            utilityRestMethods.validateQueryParams(AssetType.INSURANCE, insuranceId);
            utilityRestMethods.validateQueryParams(AssetType.RECURRING_EVENT, eventId);
            utilityRestMethods.validateQueryParams(AssetType.VEHICLE, propertyId);
        } catch (QueryParamsInvalidException expectedException) {
            Assert.fail( "Threw QueryParamsInvalidException when it shouldn't" );
        }

        try {
            utilityRestMethods.validateQueryParams(AssetType.INSURANCE, -1);
            Assert.fail( "Failed to throw QueryParamsInvalidException exception" );
        } catch (QueryParamsInvalidException expectedException) {}

        try {
            utilityRestMethods.validateQueryParams(AssetType.RECURRING_EVENT, 0);
            Assert.fail( "Failed to throw QueryParamsInvalidException exception" );
        } catch (QueryParamsInvalidException expectedException) {}

        try {
            utilityRestMethods.validateQueryParams(AssetType.REAL_ESTATE, eventId);
            Assert.fail( "Failed to throw QueryParamsInvalidException exception" );
        } catch (QueryParamsInvalidException expectedException) {}
    }

    @Test
    @Rollback
    public void testIdValidation() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);

        RecurringEvent recurringEvent = new RecurringEvent();
        recurringEvent.setTitle("Dummy");

        Payment payment = new Payment();
        payment.setName("Dummy");
        payment.setType(PaymentType.OTHER);
        payment.setValue(1.0);

        Document document = new Document();
        document.setName("Dummy");

        ownerService.createOwner(owner);
        Long insuranceId = insuranceService.createInsurance(insurance);
        Long propertyId = propertyService.createProperty(vehicle);
        Long eventId = recurringEventService.createRecurringEvent(recurringEvent);
        Long paymentId = paymentService.createPayment(payment, insuranceId, AssetType.INSURANCE);
        Long documentId = documentService.createDocument(document, propertyId, AssetType.VEHICLE);

        try {
            utilityRestMethods.validateIdExists(insuranceId, Insurance.class);
            utilityRestMethods.validateIdExists(propertyId, Property.class);
            utilityRestMethods.validateIdExists(eventId, RecurringEvent.class);
            utilityRestMethods.validateIdExists(paymentId, Payment.class);
            utilityRestMethods.validateIdExists(documentId, Document.class);
        } catch (IdNotFoundException expectedException) {
            Assert.fail( "Threw unexpected exception: IdNotFoundException" );
        }

        try {
            utilityRestMethods.validateIdExists(-1, Insurance.class);
            Assert.fail( "Failed to throw IdNotFoundException exception" );
        } catch (IdNotFoundException expectedException) {
        }

        try {
            utilityRestMethods.validateIdExists(0, RecurringEvent.class);
            Assert.fail( "Failed to throw IdNotFoundException exception" );
        } catch (IdNotFoundException expectedException) {}

        try {
            utilityRestMethods.validateIdExists(eventId, Property.class);
            Assert.fail( "Failed to throw IdNotFoundException exception" );
        } catch (IdNotFoundException expectedException) {}

        try {
            utilityRestMethods.validateIdExists(-1, Payment.class);
            Assert.fail( "Failed to throw IdNotFoundException exception" );
        } catch (IdNotFoundException expectedException) {}

        try {
            utilityRestMethods.validateIdExists(0, Document.class);
            Assert.fail( "Failed to throw IdNotFoundException exception" );
        } catch (IdNotFoundException expectedException) {}
    }

}
