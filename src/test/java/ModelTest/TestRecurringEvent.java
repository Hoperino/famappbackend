package ModelTest;

import model.RecurringEvent;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Test the equals and hashCode methods
 */
public class TestRecurringEvent {

    @Test
    public void testEqualsAndHashMethodsRecurringEvent() {
        //initialized test data
        RecurringEvent recurringEvent1 = new RecurringEvent();
        recurringEvent1.setId((long) 1);
        recurringEvent1.setTitle("Dummy");

        RecurringEvent recurringEvent2 = new RecurringEvent();
        recurringEvent2.setId((long) 2);
        recurringEvent2.setTitle("Dummy");

        RecurringEvent recurringEvent3 = new RecurringEvent();
        recurringEvent3.setId((long) 1);
        recurringEvent3.setTitle("Dummy");

        RecurringEvent recurringEvent4 = new RecurringEvent();
        recurringEvent4.setId((long) 1);
        recurringEvent4.setTitle("Dummy Diff");

        assertFalse(recurringEvent1.hashCode() == recurringEvent2.hashCode());
        assertTrue(recurringEvent1.hashCode() == recurringEvent3.hashCode());
        assertFalse(recurringEvent3.hashCode() == recurringEvent4.hashCode());
        assertTrue(recurringEvent1.equals(recurringEvent3));
        assertFalse(recurringEvent3.equals(recurringEvent4));
    }
}
