package ModelTest;

import model.Owner;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Test the equals and hashCode methods
 */
public class TestOwner {

    @Test
    public void testEqualsAndHashMethodsOwner() {
        //initialized test data
        Owner owner1 = new Owner();
        owner1.setId((long) 1);
        owner1.setFirstName("Dummy");
        owner1.setLastName("Dummy Sur");

        Owner owner2 = new Owner();
        owner2.setId((long) 2);
        owner2.setFirstName("Dummy");
        owner2.setLastName("Dummy Sur");

        Owner owner3 = new Owner();
        owner3.setId((long) 1);
        owner3.setFirstName("Dummy");
        owner3.setLastName("Dummy Sur");

        Owner owner4 = new Owner();
        owner4.setId((long) 1);
        owner4.setFirstName("Dummy");
        owner4.setLastName("Dummy Diff");

        assertFalse(owner1.hashCode() == owner2.hashCode());
        assertTrue(owner1.hashCode() == owner3.hashCode());
        assertFalse(owner3.hashCode() == owner4.hashCode());
        assertTrue(owner1.equals(owner3));
        assertFalse(owner3.equals(owner4));
    }
}
