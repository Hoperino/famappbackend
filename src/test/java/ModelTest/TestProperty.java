package ModelTest;

import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Test the equals and hashCode methods
 */
public class TestProperty {

    @Test
    public void testEqualsAndHashMethodsProperty() {
        //initialized test data
        Property property1 = new Vehicle();
        property1.setId((long) 1);
        property1.setName("Dummy");

        Property property2 = new Stock();
        property2.setId((long) 2);
        property2.setName("Dummy");

        Property property3 = new RealEstate();
        property3.setId((long) 1);
        property3.setName("Dummy");

        Property property4 = new Vehicle();
        property4.setId((long) 1);
        property4.setName("Dummy Diff");

        Property property5 = new Stock();
        property5.setId((long) 2);
        property5.setName("Dummy");

        assertFalse(property1.hashCode() == property2.hashCode());
        assertTrue(property1.hashCode() == property3.hashCode());
        assertFalse(property3.hashCode() == property4.hashCode());
        assertTrue(property2.equals(property5));
        assertFalse(property1.equals(property3));
        assertFalse(property3.equals(property4));
    }
}
