package ModelTest;

import model.Payment;
import model.utils.PaymentType;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Test the equals and hashCode methods
 */
public class TestPayment {

    @Test
    public void testEqualsAndHashMethodsRecurringEvent() {
        //initialized test data
        Payment payment1 = new Payment();
        payment1.setId((long) 1);
        payment1.setName("Dummy");
        payment1.setValue(1.0);
        payment1.setType(PaymentType.OTHER);


        Payment payment2 = new Payment();
        payment2.setId((long) 2);
        payment2.setName("Dummy");
        payment2.setValue(1.0);
        payment2.setType(PaymentType.OTHER);

        Payment payment3 = new Payment();
        payment3.setId((long) 1);
        payment3.setName("Dummy");
        payment3.setValue(1.0);
        payment3.setType(PaymentType.OTHER);

        Payment payment4 = new Payment();
        payment4.setId((long) 1);
        payment4.setName("Dummy Diff");
        payment4.setValue(2.0);
        payment4.setType(PaymentType.TAX);

        assertFalse(payment1.hashCode() == payment2.hashCode());
        assertTrue(payment1.hashCode() == payment3.hashCode());
        assertFalse(payment3.hashCode() == payment4.hashCode());
        assertTrue(payment1.equals(payment3));
        assertFalse(payment3.equals(payment4));
    }
}
