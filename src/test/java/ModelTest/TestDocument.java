package ModelTest;

import model.Document;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Test the equals and hashCode methods
 */
public class TestDocument {

    @Test
    public void testEqualsAndHashMethodsRecurringEvent() {
        //initialized test data
        Document document1 = new Document();
        document1.setId((long) 1);
        document1.setName("Dummy");

        Document document2 = new Document();
        document2.setId((long) 2);
        document2.setName("Dummy");

        Document document3 = new Document();
        document3.setId((long) 1);
        document3.setName("Dummy");

        Document document4 = new Document();
        document4.setId((long) 1);
        document4.setName("Dummy Diff");

        assertFalse(document1.hashCode() == document2.hashCode());
        assertTrue(document1.hashCode() == document3.hashCode());
        assertFalse(document3.hashCode() == document4.hashCode());
        assertTrue(document1.equals(document3));
        assertFalse(document3.equals(document4));
    }
}
