package ModelTest;

import model.Insurance;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

/**
 * Test the equals and hashCode methods
 */
public class TestInsurance {

    @Test
    public void testEqualsAndHashMethodsInsurance() {
        //initialized test data
        Insurance insurance1 = new Insurance();
        insurance1.setId((long) 1);
        insurance1.setUid("Dummy");
        insurance1.setName("Dummy Sur");

        Insurance insurance2 = new Insurance();
        insurance2.setId((long) 2);
        insurance2.setUid("Dummy");
        insurance2.setName("Dummy Sur");

        Insurance insurance3 = new Insurance();
        insurance3.setId((long) 1);
        insurance3.setUid("Dummy");
        insurance3.setName("Dummy Sur");

        Insurance insurance4 = new Insurance();
        insurance4.setId((long) 1);
        insurance4.setUid("Dummy");
        insurance4.setName("Dummy Diff");

        assertFalse(insurance1.hashCode() == insurance2.hashCode());
        assertTrue(insurance1.hashCode() == insurance3.hashCode());
        assertFalse(insurance3.hashCode() == insurance4.hashCode());
        assertTrue(insurance1.equals(insurance3));
        assertFalse(insurance3.equals(insurance4));
    }
}
