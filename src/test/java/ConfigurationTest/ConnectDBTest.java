package ConfigurationTest;

import config.DaoConfig;
import config.HibernateConfig;
import model.Avatar;
import model.Owner;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Test connection to DB
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@TestPropertySource(properties = {
        "jdbc.mysql.driverClassName=com.mysql.jdbc.Driver",
        "jdbc.mysql.url=jdbc:mysql://localhost:3306/organizer",
        "jdbc.mysql.user=root",
        "jdbc.mysql.pass=",
        "hibernate.mysql.dialect=org.hibernate.dialect.MySQLDialect",
        "hibernate.mysql.show_sql=false",
        "hibernate.mysql.hbm2ddl.auto=create"
})
public class ConnectDBTest {

    @Autowired
    private HibernateConfig hibernateConfig;

    @Autowired
    private DaoConfig daoConfig;


    @Test
    public void connectionToDb() {
        assertNotNull(hibernateConfig);
        assertNotNull(daoConfig);

        SessionFactory sessionFactory = (SessionFactory) hibernateConfig.getLocalSessionFactory().getObject();
        assertNotNull(sessionFactory);
        assertTrue(!sessionFactory.isClosed());
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()){
            assertTrue(session.isOpen());
            tx = session.beginTransaction();
            tx.commit();
        }catch (HibernateException e){
            assert tx != null;
            tx.rollback();
        }
    }
}
