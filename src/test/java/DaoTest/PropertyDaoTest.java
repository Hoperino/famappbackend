package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.*;
import model.Owner;
import model.property.Property;
import model.property.RealEstate;
import model.property.Stock;
import model.property.Vehicle;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing Stock DAO, Vehicle DAO and Real Estate DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class PropertyDaoTest {

    @Autowired
    private OwnerDAO ownerDAO;

    @Autowired
    private VehicleDAO vehicleDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private RealEstateDAO realEstateDAO;

    @Autowired
    private PropertyDAO propertyDAO;

    @Test
    @Rollback
    public void doCrudPropertyTest() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");
        ownerDAO.add(owner);

        /** Test out All of the property DAO methods */
        // create
        Vehicle propertyVehicle = new Vehicle();
        propertyVehicle.setName("DummyProp");
        propertyVehicle.setOwner(owner);
        vehicleDAO.add(propertyVehicle);

        RealEstate propertyRealEstate = new RealEstate();
        propertyRealEstate.setName("DummyProp2");
        propertyRealEstate.setOwner(owner);
        realEstateDAO.add(propertyRealEstate);

        // read
        List<Vehicle> vehicleList = new ArrayList<>(vehicleDAO.getAll());
        List<RealEstate> realEstateList = new ArrayList<>(realEstateDAO.getAll());

        Assert.assertTrue(vehicleList.size() == 1 && realEstateList.size() == 1);

        Vehicle vehicleFromPropertyDao = (Vehicle) propertyDAO.findProperty(vehicleList.get(0).getId());
        RealEstate realEstateFromPropertyDao = (RealEstate) propertyDAO.findProperty(realEstateList.get(0).getId());

        Assert.assertNotNull(vehicleFromPropertyDao);
        Assert.assertNotNull(realEstateFromPropertyDao);
        Assert.assertTrue(propertyVehicle.equals(vehicleFromPropertyDao));
        Assert.assertTrue(propertyRealEstate.equals(realEstateFromPropertyDao));

        // update
        propertyVehicle.setName("UpdatedName");
        propertyRealEstate.setName("UpdatedName");
        propertyDAO.updateProperty(propertyVehicle);
        propertyDAO.updateProperty(propertyRealEstate);

        vehicleList = new ArrayList<>(vehicleDAO.getAll());
        realEstateList = new ArrayList<>(realEstateDAO.getAll());

        Assert.assertTrue(vehicleList.size() == 1 && realEstateList.size() == 1);
        Assert.assertTrue(propertyVehicle.getName().equals(vehicleList.get(0).getName()));
        Assert.assertTrue(propertyRealEstate.getName().equals(realEstateList.get(0).getName()));

        // delete
        propertyDAO.removeProperty(vehicleList.get(0).getId());
        propertyDAO.removeProperty(realEstateList.get(0).getId());

        vehicleList = new ArrayList<>(vehicleDAO.getAll());
        realEstateList = new ArrayList<>(realEstateDAO.getAll());
        Assert.assertTrue(vehicleList.isEmpty() && realEstateList.isEmpty());

        /** Test out All of the property subtype's DAOs */
        // create
        Long vehicleId = null;
        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);

        Long realEstateId = null;
        RealEstate realEstate = new RealEstate();
        realEstate.setName("Dummy");
        realEstate.setOwner(owner);

        Long stockId = null;
        Stock stock = new Stock();
        stock.setName("Dummy");
        stock.setOwner(owner);

        Assert.assertTrue(TestTransaction.isActive());

        vehicleId = vehicleDAO.add(vehicle);
        realEstateId = realEstateDAO.add(realEstate);
        stockId = stockDAO.add(stock);

        List<Vehicle> vehicleSet = new ArrayList<>(vehicleDAO.getAll());
        Assert.assertTrue(vehicleSet.size() == 1);

        List<RealEstate> realEstateSet = new ArrayList<>(realEstateDAO.getAll());
        Assert.assertTrue(realEstateSet.size() == 1);

        List<Stock> stockSet = new ArrayList<>(stockDAO.getAll());
        Assert.assertTrue(stockSet.size() == 1);

        // read
        Vehicle foundByIdVehicle = vehicleDAO.findById(vehicleId);
        Assert.assertNotNull(foundByIdVehicle);
        Assert.assertTrue(foundByIdVehicle.getName().equals(vehicle.getName()));

        RealEstate foundByIdRealEstate = realEstateDAO.findById(realEstateId);
        Assert.assertNotNull(foundByIdRealEstate);
        Assert.assertTrue(foundByIdRealEstate.getName().equals(realEstate.getName()));

        Stock foundByIdStock = stockDAO.findById(stockId);
        Assert.assertNotNull(foundByIdStock);
        Assert.assertTrue(foundByIdStock.getName().equals(stock.getName()));

        // update
        vehicle.setName("Dummy Changed");
        vehicleDAO.update(vehicle);

        vehicleSet = new ArrayList<>(vehicleDAO.getAll());
        Assert.assertFalse(vehicleSet.isEmpty());
        Assert.assertTrue(vehicleSet.get(0).getName().equals(vehicle.getName()));

        realEstate.setName("Dummy Changed");
        realEstateDAO.update(realEstate);

        realEstateSet = new ArrayList<>(realEstateDAO.getAll());
        Assert.assertFalse(realEstateSet.isEmpty());
        Assert.assertTrue(realEstateSet.get(0).getName().equals(realEstate.getName()));

        stock.setName("Dummy Changed");
        stockDAO.update(stock);

        stockSet = new ArrayList<>(stockDAO.getAll());
        Assert.assertFalse(stockSet.isEmpty());
        Assert.assertTrue(stockSet.get(0).getName().equals(stock.getName()));

        // delete
        vehicleDAO.delete(vehicle);
        vehicleSet = new ArrayList<>(vehicleDAO.getAll());
        Assert.assertTrue(vehicleSet.isEmpty());

        realEstateDAO.delete(realEstate);
        realEstateSet = new ArrayList<>(realEstateDAO.getAll());
        Assert.assertTrue(realEstateSet.isEmpty());

        stockDAO.delete(stock);
        stockSet = new ArrayList<>(stockDAO.getAll());
        Assert.assertTrue(stockSet.isEmpty());
    }
}
