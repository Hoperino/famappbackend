package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.AvatarDAO;
import databasedao.interfaces.OwnerDAO;
import model.Avatar;
import model.Owner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing Avatar DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class AvatarDaoTest {

    @Autowired
    private AvatarDAO avatarDAO;

    @Autowired
    private OwnerDAO ownerDAO;

    @Test
    @Rollback
    public void doCrudAvatar() {
        // create -- creating Avatar id done through creating of owner due to CascadeType.ALL relationship Owner -> Avatar
        Avatar avatar = new Avatar();
        avatar.setImage("image".getBytes());
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");
        owner.setAvatar(avatar);

        Assert.assertTrue(TestTransaction.isActive());
        ownerDAO.add(owner);

        List<Owner> ownerSet = new ArrayList<>(ownerDAO.getAll());
        Assert.assertFalse(ownerSet.isEmpty());
        List<Avatar> avatarSet = new ArrayList<>(avatarDAO.getAll());
        Assert.assertFalse(avatarSet.isEmpty());

        // read
        Avatar foundById = avatarDAO.findById(ownerSet.get(0).getAvatar().getId());
        Assert.assertNotNull(foundById);

        /*
         * update -- updating Avatar is done through updating of owner
         * due to CascadeType.ALL relationship Owner -> Avatar
         */
        avatar.setImage("Dummy Changed".getBytes());
        owner.setAvatar(avatar);
        ownerDAO.update(owner);

        /*
         * delete -- deleting Avatar is done by removing association with owner
         * and updating owner due to CascadeType.All relationship Owner -> Avatar
         */
        owner.setAvatar(null);
        ownerDAO.update(owner);
        avatarSet = new ArrayList<>(avatarDAO.getAll());
        Assert.assertTrue(avatarSet.isEmpty());
    }
}
