package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.*;
import model.Document;
import model.Insurance;
import model.Owner;
import model.RecurringEvent;
import model.property.Vehicle;
import model.utils.DocumentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Test Document DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class DocumentDaoTest {

    @Autowired
    private OwnerDAO ownerDAO;

    @Autowired
    private DocumentDAO documentDao;

    @Autowired
    private VehicleDAO vehicleDAO;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Autowired
    private RecurringEventDAO recurringEventDAO;

    @Test
    @Rollback
    public void doCrudPaymentTest() {
        //init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");
        ownerDAO.add(owner);

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);
        vehicleDAO.add(vehicle);

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);
        insuranceDAO.add(insurance);

        RecurringEvent event = new RecurringEvent();
        event.setTitle("Dummy");
        recurringEventDAO.add(event);

        // create
        Long documentId = null;
        Document document = new Document();
        document.setName("Dummy");
        document.setType(DocumentType.DESCRIPTIVE);

        Assert.assertTrue(TestTransaction.isActive());
        documentId = documentDao.add(document);

        List<Document> documentList = new ArrayList<>(documentDao.getAll());
        Assert.assertFalse(documentList.isEmpty());
        Assert.assertTrue(documentList.get(0).getName().equals(document.getName()));

        // read
        Document foundById = documentDao.findById(documentId);
        Assert.assertNotNull(foundById);
        Assert.assertTrue(foundById.getName().equals(document.getName()));

        // update
        document.setName("Changed Dummy");
        documentDao.update(document);
        documentList = new ArrayList<>(documentDao.getAll());
        Assert.assertFalse(documentList.isEmpty());
        Assert.assertTrue(documentList.get(0).getName().equals(document.getName()));

        // delete
        documentDao.delete(document);
        documentList = new ArrayList<>(documentDao.getAll());
        Assert.assertTrue(documentList.isEmpty());
    }
}
