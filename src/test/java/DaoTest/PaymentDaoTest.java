package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.*;
import model.Insurance;
import model.Owner;
import model.Payment;
import model.RecurringEvent;
import model.property.Property;
import model.property.RealEstate;
import model.property.Vehicle;
import model.utils.PaymentType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Test Payment DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class PaymentDaoTest {

    @Autowired
    private OwnerDAO ownerDAO;

    @Autowired
    private PaymentDAO paymentDAO;

    @Autowired
    private VehicleDAO vehicleDAO;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Autowired
    private RecurringEventDAO recurringEventDAO;

    @Test
    @Rollback
    public void doCrudPaymentTest() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");
        ownerDAO.add(owner);

        Vehicle vehicle = new Vehicle();
        vehicle.setName("Dummy");
        vehicle.setOwner(owner);
        vehicleDAO.add(vehicle);

        Insurance insurance = new Insurance();
        insurance.setUid("Dummy");
        insurance.setName("Dummy");
        insurance.setOwner(owner);
        insuranceDAO.add(insurance);

        RecurringEvent event = new RecurringEvent();
        event.setTitle("Dummy");
        recurringEventDAO.add(event);

        // create
        Long paymentId = null;
        Payment payment = new Payment();
        payment.setName("Dummy");
        payment.setValue(1.0);
        payment.setType(PaymentType.OTHER);

        Assert.assertTrue(TestTransaction.isActive());
        paymentId = paymentDAO.add(payment);

        List<Payment> paymentList = new ArrayList<>(paymentDAO.getAll());
        Assert.assertFalse(paymentList.isEmpty());
        Assert.assertTrue(paymentList.get(0).getName().equals(payment.getName()));

        // read
        Payment foundById = paymentDAO.findById(paymentId);
        Assert.assertNotNull(foundById);
        Assert.assertTrue(foundById.getName().equals(payment.getName()));

        // update
        payment.setName("Changed Dummy");
        paymentDAO.update(payment);
        paymentList = new ArrayList<>(paymentDAO.getAll());
        Assert.assertFalse(paymentList.isEmpty());
        Assert.assertTrue(paymentList.get(0).getName().equals(payment.getName()));

//        //add payment for property
//        List<Property> vehiclePropertySet = new ArrayList<>(vehicleDAO.getAll());
//        Assert.assertTrue(vehiclePropertySet.get(0).getPaymentSet().isEmpty());
//        paymentDAO.addPaymentToProperty(vehicle.getId(), payment.getId());
//        vehiclePropertySet = new ArrayList<>(vehicleDAO.getAll());
//        Assert.assertTrue(vehiclePropertySet.get(0).getPaymentSet().size() == 1);
//
//        //get all payments for property
//        List<Payment> paymentSetForProperty = new ArrayList<>(paymentDAO.getAllPaymentsForProperty(vehicle.getId()));
//        Assert.assertTrue(paymentSetForProperty.size() == 1);
//        Assert.assertTrue(((long) paymentSetForProperty.get(0).getId()) == payment.getId());
//
//        //remove payment association from property
//        paymentDAO.removePaymentFromProperty(vehicle.getId(), payment.getId());
//        paymentSetForProperty = new ArrayList<>(paymentDAO.getAllPaymentsForProperty(vehicle.getId()));
//        Assert.assertTrue(paymentSetForProperty.isEmpty());
//
//        //add payment for insurance
//        List<Insurance> insuranceSet = new ArrayList<>(insuranceDAO.getAll());
//        Assert.assertTrue(insuranceSet.get(0).getPaymentSet().isEmpty());
//        paymentDAO.addPaymentToInsurance(insurance.getId(), payment.getId());
//        insuranceSet = new ArrayList<>(insuranceDAO.getAll());
//        Assert.assertTrue(insuranceSet.get(0).getPaymentSet().size() == 1);
//
//        //get all payments for insurance
//        List<Payment> paymentSetForInsurance = new ArrayList<>(paymentDAO.getAllPaymentsForInsurance(insurance.getId()));
//        Assert.assertTrue(paymentSetForInsurance.size() == 1);
//        Assert.assertTrue(((long) paymentSetForInsurance.get(0).getId()) == payment.getId());
//
//        //remove payment association from insurance
//        paymentDAO.removePaymentFromInsurance(insurance.getId(), payment.getId());
//        paymentSetForInsurance = new ArrayList<>(paymentDAO.getAllPaymentsForInsurance(insurance.getId()));
//        Assert.assertTrue(paymentSetForInsurance.isEmpty());
//
//        //add payment for recurring event
//        List<RecurringEvent> eventSet = new ArrayList<>(recurringEventDAO.getAll());
//        Assert.assertTrue(eventSet.get(0).getPaymentSet().isEmpty());
//        paymentDAO.addPaymentToRecurringEvent(event.getId(), payment.getId());
//        eventSet = new ArrayList<>(recurringEventDAO.getAll());
//        Assert.assertTrue(eventSet.get(0).getPaymentSet().size() == 1);
//
//        //get all payments for recurring event
//        List<Payment> paymentSetForEvent = new ArrayList<>(paymentDAO.getAllPaymentsForRecurringEvent(event.getId()));
//        Assert.assertTrue(paymentSetForEvent.size() == 1);
//        Assert.assertTrue(((long) paymentSetForEvent.get(0).getId()) == payment.getId());
//
//        //remove payment association from insurance
//        paymentDAO.removePaymentFromRecurringEvent(event.getId(), payment.getId());
//        paymentSetForEvent = new ArrayList<>(paymentDAO.getAllPaymentsForRecurringEvent(event.getId()));
//        Assert.assertTrue(paymentSetForEvent.isEmpty());

        // delete
        paymentDAO.delete(payment);
        paymentList = new ArrayList<>(paymentDAO.getAll());
        Assert.assertTrue(paymentList.isEmpty());
    }
}
