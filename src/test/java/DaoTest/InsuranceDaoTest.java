package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.InsuranceDAO;
import databasedao.interfaces.OwnerDAO;
import model.Insurance;
import model.Owner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing Insurance DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class InsuranceDaoTest {

    @Autowired
    private OwnerDAO ownerDAO;

    @Autowired
    private InsuranceDAO insuranceDAO;

    @Test
    @Rollback
    public void doCrudInsuranceTest() {
        // init
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");
        ownerDAO.add(owner);

        // create
        Long insuranceId = null;
        Insurance insurance = new Insurance();
        insurance.setUid("UID");
        insurance.setName("Dummy");
        insurance.setOwner(owner);

        Assert.assertTrue(TestTransaction.isActive());
        insuranceId = insuranceDAO.add(insurance);

        List<Insurance> insuranceSet = new ArrayList<>(insuranceDAO.getAll());
        Assert.assertFalse(insuranceSet.isEmpty());
        Assert.assertTrue(insuranceSet.get(0).getName().equals(insurance.getName()));

        // read
        Insurance foundById = insuranceDAO.findById(insuranceId);
        Assert.assertNotNull(foundById);
        Assert.assertTrue(foundById.getName().equals(insurance.getName()));

        // update
        insurance.setName("Dummy Changed");
        insuranceDAO.update(insurance);

        insuranceSet = new ArrayList<>(insuranceDAO.getAll());
        Assert.assertFalse(insuranceSet.isEmpty());
        Assert.assertTrue(insuranceSet.get(0).getName().equals("Dummy Changed"));

        // delete
        insuranceDAO.delete(insurance);
        insuranceSet = new ArrayList<>(insuranceDAO.getAll());
        Assert.assertTrue(insuranceSet.isEmpty());
    }
}
