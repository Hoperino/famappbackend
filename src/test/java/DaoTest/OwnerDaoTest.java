package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.OwnerDAO;
import model.Owner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing Owner DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class OwnerDaoTest {

    @Autowired
    private OwnerDAO ownerDAO;

    @Test
    @Rollback
    public void doCrudOwnerTest() {
        // create
        Long id = null;
        Owner owner = new Owner();
        owner.setFirstName("Dummy");
        owner.setLastName("Dummy");

        Assert.assertTrue(TestTransaction.isActive());
        id = ownerDAO.add(owner);

        List<Owner> ownerSet = new ArrayList<>(ownerDAO.getAll());
        Assert.assertFalse(ownerSet.isEmpty());
        Assert.assertTrue(ownerSet.get(0).getFirstName().equals(owner.getFirstName()));

        // read
        Owner foundById = ownerDAO.findById(id);
        Assert.assertNotNull(foundById);
        Assert.assertTrue(foundById.getFirstName().equals(owner.getFirstName()));

        // update
        owner.setFirstName("Dummy Changed");
        ownerDAO.update(owner);

        ownerSet = new ArrayList<>(ownerDAO.getAll());
        Assert.assertFalse(ownerSet.isEmpty());
        Assert.assertTrue(ownerSet.get(0).getFirstName().equals(owner.getFirstName()));

        // delete
        ownerDAO.delete(owner);
        ownerSet = new ArrayList<>(ownerDAO.getAll());
        Assert.assertTrue(ownerSet.isEmpty());
    }
}
