package DaoTest;

import config.DaoConfig;
import config.HibernateConfig;
import databasedao.interfaces.RecurringEventDAO;
import model.RecurringEvent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing Recurring Event DAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class, DaoConfig.class})
@Transactional
public class RecurringEventDaoTest {

    @Autowired
    private RecurringEventDAO recurringEventDao;

    @Test
    @Rollback
    public void doCrudRecurringEventTest() {
        // create
        Long id = null;
        RecurringEvent recurringEvent = new RecurringEvent();
        recurringEvent.setTitle("Dummy");

        Assert.assertTrue(TestTransaction.isActive());
        id = recurringEventDao.add(recurringEvent);

        List<RecurringEvent> eventSet = new ArrayList<>(recurringEventDao.getAll());
        Assert.assertFalse(eventSet.isEmpty());
        Assert.assertTrue(eventSet.get(0).getTitle().equals(recurringEvent.getTitle()));

        // read
        RecurringEvent foundById = recurringEventDao.findById(id);
        Assert.assertNotNull(foundById);
        Assert.assertTrue(foundById.getTitle().equals(recurringEvent.getTitle()));

        // update
        recurringEvent.setTitle("Dummy Changed");
        recurringEventDao.update(recurringEvent);

        eventSet = new ArrayList<>(recurringEventDao.getAll());
        Assert.assertFalse(eventSet.isEmpty());
        Assert.assertTrue(eventSet.get(0).getTitle().equals(recurringEvent.getTitle()));

        // delete
        recurringEventDao.delete(recurringEvent);
        eventSet = new ArrayList<>(recurringEventDao.getAll());
        Assert.assertTrue(eventSet.isEmpty());
    }
}
